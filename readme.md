Lightweight web-components (and custom javascript API sdk) to
communicate with [matrix](https://matrix.org/) (public) homeservers,
rooms, spaces, users, events.

- [documentation](./docs/) for the project
- [examples](./examples/) of each components usage
- [website](https://sctlib.gitlab.io/mwc) with the rendered examples
- [npm](https://www.npmjs.com/package/@sctlib/mwc) package
- [gitlab](https://gitlab.com/sctlib/mwc) code
- [#mwc.sctlib:matrix.org](https://matrix.to/#/#mwc.sctlib:matrix.org) chat

> This project is in prototype state, and its APIs are subject to
> changes. Feel free to discuss features and bugs in git issues or the
> matrix room(s).

# Example usage

In an HTML file, we can use the following code to display a matrix room (here by its alias, and works with its ID too).

```html
<script type="module" src="https://unpkg.com/@sctlib/mwc"></script>
<matrix-room profile-id="#sctlib-mwc:matrix.org"></matrix-room>
```
