import matrixApi from "../services/api.js";

/* Example to start with a fresh template */
export default class MatrixElement extends HTMLElement {
	constructor() {
		super();
		this.api = matrixApi;
	}
	async connectedCallback() {
		try {
			this.exampleData = await this.api.newTxnId();
			this.exampleProfile = await this.api.checkMatrixId("#libli:matrix.org");
		} catch (error) {
			console.log(error);
		}
		this.render();
	}
	render() {
		const $data = this.createData(this.exampleData);
		const $profile = this.createProfile(this.exampleProfile);
		this.replaceChildren($data, $profile);
	}
	createData(data) {
		const $data = document.createElement("matrix-element-data");
		$data.textContent = data;
		return $data;
	}
	createProfile(profile) {
		const $info = document.createElement("matrix-element-profile");
		$info.textContent = JSON.stringify(profile, null, 2);
		return $info;
	}
}
