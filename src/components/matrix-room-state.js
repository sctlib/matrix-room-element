export default class MatrixEvent extends HTMLElement {
	static get observedAttributes() {
		return [
			"flag-sanitizer",
			"origin",
			"state",
			"widgets",
			"space-children",
			"space-parents",
		];
	}
	get flagSanitizer() {
		return this.getAttribute("flag-sanitizer") === "true";
	}
	get origin() {
		return this.getAttribute("origin");
	}
	get state() {
		return JSON.parse(this.getAttribute("state")) || {};
	}
	get widgets() {
		return JSON.parse(this.getAttribute("widgets"));
	}
	get spaceChildren() {
		return JSON.parse(this.getAttribute("space-children"));
	}
	get spaceParents() {
		return JSON.parse(this.getAttribute("space-parents"));
	}

	attributeChangedCallback() {
		this.render();
	}

	connectedCallback() {
		this.render();
	}

	/* Find all URLs in plain text, return text with <a href="URL"/>
		 Docs: https://stackoverflow.com/a/1500501 */
	urlify(text) {
		const urlRegex = /(https?:\/\/[^\s]+)/g;
		return text.replace(urlRegex, (url) => {
			return `<a href="${url}" rel="noreferrer noopener" target="_blank">${url}</a>`;
		});
	}

	render() {
		this.innerHTML = "";
		const {
			alias,
			guest_access,
			history_visibility,
			join_rule,
			name,
			topic,
			avatar,
		} = this.state;

		/* first, insert space parents, if any */
		this.spaceParents?.length && this.renderSpaceParents();

		if (name) {
			const $name = document.createElement("matrix-room-name");
			$name.innerText = name;
			this.append($name);
		}

		if (alias) {
			const $alias = document.createElement("matrix-room-alias");
			if (this.origin) {
				const $link = document.createElement("a");
				$link.href = `${this.origin}/${alias}`;
				$link.innerText = alias;
				$alias.append($link);
			} else {
				const $link = document.createElement("span");
				$link.innerText = alias;
				$alias.append($link);
			}
			this.append($alias);
		}

		if (avatar) {
			const $avatar = document.createElement("matrix-image");
			$avatar.setAttribute("mxc", avatar);
			this.append($avatar);
		}

		if (topic) {
			const $topic = document.createElement("matrix-room-topic");
			if (
				this.flagSanitizer &&
				Sanitizer &&
				typeof Sanitizer.sanitize === "function"
			) {
				$topic.innerHTML = Sanitizer.sanitize(this.urlify(topic));
			} else {
				$topic.innerText = topic;
			}
			this.append($topic);
		}

		/* finnally render space children */
		this.spaceChildren?.length && this.renderSpaceChildren();
	}
	renderSpaceChildren() {
		const $spaceChildren = document.createElement("matrix-space-children");
		const $spaceChildrenMenu = document.createElement("menu");
		$spaceChildren.append($spaceChildrenMenu);

		const spaceChildren = this.spaceChildren.filter((spaceChild) => {
			/* seems "valid" rooms children of space have no `replaces_state` or `content`
			 can't really figure it out from the docs */
			if (spaceChild.replaces_state && !Object.keys(spaceChild.content).length) {
				return false;
			} else {
				return true;
			}
		});
		spaceChildren.forEach((space) => {
			const $spaceChildLi = document.createElement("li");
			const $spaceChild = document.createElement("matrix-room");
			$spaceChild.setAttribute("profile-id", space.state_key);
			$spaceChild.setAttribute("show-context", false);
			$spaceChild.setAttribute("hide-widgets", true);
			$spaceChild.setAttribute("show-space-children", true);
			if (this.flagSanitizer && typeof Sanitizer === "function") {
				$spaceChild.setAttribute("flag-sanitizer", this.flagSanitizer);
			}
			if (this.origin) {
				$spaceChild.setAttribute("origin", this.origin);
			}
			$spaceChildLi.append($spaceChild);
			$spaceChildrenMenu.append($spaceChildLi);
		});
		this.append($spaceChildren);
	}
	renderSpaceParents() {
		const $spaceParents = document.createElement("matrix-space-parents");
		const spaceParents = this.spaceParents.filter((spaceParent) => {
			/* `replaces_state` seams to mean "the event is replaced by"  */
			if (spaceParent.replaces_state) {
				return false;
			} else {
				return true;
			}
		});
		spaceParents.forEach((space) => {
			const $spaceParent = document.createElement("matrix-room");
			$spaceParent.setAttribute("profile-id", space.state_key);
			$spaceParent.setAttribute("show-context", false);
			$spaceParent.setAttribute("hide-widgets", true);
			$spaceParent.setAttribute("show-space-children", false);
			if (this.flagSanitizer && typeof Sanitizer === "function") {
				$spaceParent.setAttribute("flag-sanitizer", this.flagSanitizer);
			}
			if (this.origin) {
				$spaceParent.setAttribute("origin", this.origin);
			}
			$spaceParents.append($spaceParent);
		});
		this.append($spaceParents);
	}
}
