import matrixApi from "../services/api.js";
import MX_DOM_EVENTS from "../services/dom-events.js";
const { ON_REGISTER_AVAILABLE } = MX_DOM_EVENTS;

export default class MatrixRegisterUsernameAvailable extends HTMLElement {
	stage = "mwc.register.username.avaialble";
	static get observedAttributes() {
		return ["username", "homeserver"];
	}
	get username() {
		return this.getAttribute("username");
	}
	set username(str) {
		this.setAttribute("username", str.trim());
	}
	get homeserver() {
		return this.getAttribute("homeserver");
	}

	/* helpers */
	get userId() {
		return this.api.buildProfileAddress("@", this.username, this.homeserver);
	}
	get $fieldsets() {
		return this.querySelectorAll("fieldset");
	}
	get $submitOutput() {
		return this.querySelector("output");
	}
	disableForm() {
		this.$fieldsets.forEach(($fieldset) => {
			$fieldset.setAttribute("disabled", true);
		});
	}
	enableForm() {
		this.$fieldsets.forEach(($fieldset) => {
			$fieldset.removeAttribute("disabled");
		});
	}

	/* events handler */
	async onSubmit(event) {
		event.preventDefault();
		event.stopPropagation();
		const formData = new FormData(event.target);
		this.username = formData.get("username");
		this.disableForm();
		if (this.username && this.homeserver) {
			await this.checkUsernameAvailable(this.userId);
		}
		this.enableForm();
		this.render();
		if (this.res?.available) {
			this.dispatchEvent(
				new CustomEvent(ON_REGISTER_AVAILABLE, {
					bubbles: true,
					detail: this.username,
				})
			);
		}
	}

	/* methods */
	async checkUsernameAvailable(username) {
		this.error = null;
		this.res = null;
		try {
			const res = await this.api.registerAvailable(username);
			if (res.error) {
				throw res;
			} else {
				this.res = res;
			}
		} catch (error) {
			console.log("error", error);
			this.error = error;
		}
	}

	/* lyfecycle */
	connectedCallback() {
		this.api = matrixApi;
		this.render();
	}
	disconnectedCallback() {
		if (this.$form) {
			this.$form.removeEventListener("submit", this.onSubmit.bind(this));
		}
	}
	/* render DOM */
	render() {
		if (this.error) {
			const $error = this.createError(this.error);
			this.$submitOutput.replaceChildren($error);
		} else {
			const $form = this.createForm(this.username, this.homeserver);
			this.replaceChildren($form);
			if (this.res?.available) {
				this.append(
					this.createAvailable(this.username, this.homeserver, this.userId)
				);
			}
		}
	}
	createError(error) {
		const $error = document.createElement("matrix-error");
		$error.setAttribute("error", JSON.stringify(error));
		return $error;
	}
	createAvailable(username, homeserver, userId) {
		const $available = document.createElement("p");
		const $username = document.createElement("code");
		$username.textContent = username;

		const $userId = document.createElement("code");
		$userId.textContent = userId;

		$available.append(
			"The username ",
			$username,
			" is available to register as user_id ",
			$userId,
			" on the ",
			homeserver,
			" homeserver"
		);
		return $available;
	}
	createForm(username, homeserver) {
		const $form = document.createElement("form");
		$form.addEventListener("submit", this.onSubmit.bind(this));
		$form.append(
			this.createUsername(username, homeserver),
			this.createSubmit()
		);
		return $form;
	}
	createUsername(username, homeserver) {
		const $userField = document.createElement("fieldset");
		const $userLegend = document.createElement("legend");
		$userLegend.innerText = `Username (to create: @username:${homeserver})`;
		const $user = document.createElement("input");
		$user.type = "username";
		$user.name = "username";
		$user.placeholder = "Matrix username";
		$user.required = true;
		if (username) {
			$user.value = username;
		}
		$userField.append($userLegend, $user);
		return $userField;
	}
	createSubmit() {
		const $submitField = document.createElement("fieldset");
		const $submit = document.createElement("button");
		$submit.textContent = "Check username availability";
		$submit.type = "submit";

		const $submitOutput = document.createElement("output");
		$submitField.append($submit, $submitOutput);
		return $submitField;
	}
}
