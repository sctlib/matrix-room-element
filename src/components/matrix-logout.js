import matrixApi from "../services/api.js";

export default class MatrixLogout extends HTMLElement {
	/* helpers */
	get $fieldset() {
		return this.querySelector("fieldset");
	}
	disableForm() {
		this.$fieldset.setAttribute("disabled", true);
		this.setAttribute("loading", true);
	}
	enableForm() {
		this.$fieldset.removeAttribute("disabled");
		this.removeAttribute("loading");
	}

	/* events */
	async handleSubmit(event) {
		event.preventDefault();
		this.disableForm();
		try {
			const res = await this.api.logout();
			if (res && res.error) {
				throw res;
			}
		} catch (error) {
			console.log("error during logout", error);
		}

		const logoutEvent = new CustomEvent("logout", {
			bubbles: true,
		});
		this.dispatchEvent(logoutEvent);
		this.enableForm();
	}

	/* lifecycle */
	connectedCallback() {
		this.api = matrixApi;
		this.render();
	}

	/* dom */
	render(auth = this.api.authAny, user = this.api.user) {
		this.replaceChildren();
		const $form = this.createForm(auth, user);
		this.append($form);
	}
	createForm(auth, user) {
		const $form = document.createElement("form");
		$form.addEventListener("submit", this.handleSubmit.bind(this));

		const $fieldset = document.createElement("fieldset");
		const $legend = document.createElement("legend");
		if (user) {
			$legend.innerText = `Logout ${user.user_id}`;
		} else {
			$legend.innerText = "Logout matrix user";
		}

		const $button = document.createElement("button");
		$button.innerText = "Logout";
		$button.type = "submit";
		$fieldset.append($legend, $button);

		if (!auth) {
			$fieldset.setAttribute("disabled", true);
		}

		$form.append($fieldset);
		return $form;
	}
}
