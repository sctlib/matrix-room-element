import matrixApi from "../services/api.js";
import MX_DOM_EVENTS from "../services/dom-events.js";
const { ON_LOGIN, ON_WINDOW_MESSAGE, ON_AUTH_STAGE } = MX_DOM_EVENTS;

const HS_AUTH_DONE = "authDone";

/* to render a component or a fallback;
	 "dummy" is empty, to "auto submit the step" */
const KNOWN_AUTH_STAGES = new Map([
	[
		"m.login.password",
		{
			element: "matrix-login",
			event: ON_LOGIN,
		},
	],
	[
		"m.login.email.identity",
		{
			element: "matrix-register-email-token",
			event: ON_AUTH_STAGE,
		},
	],
	[
		/* https://spec.matrix.org/v1.10/client-server-api/#dummy-auth
			 servers do-not-require any form of UIA */
		"m.login.dummy",
	],
]);

/* https://spec.matrix.org/v1.9/client-server-api/#fallback */
const UNUSED_unkown_auth_stages = [
	"m.login.recaptcha", // handled by fallback (complex, requires key?)
	"m.login.registration_token", // do we need
	"m.login.sso", // do we need
	"m.login.msisdn", // what is?
];

/* Example to start with a fresh template */
export default class MatrixAuthStage extends HTMLElement {
	static get observedAttributes() {
		return ["base-url", "stage", "session", "params", "completed"];
	}
	get baseUrl() {
		return this.getAttribute("base-url");
	}
	get stage() {
		return this.getAttribute("stage");
	}
	get session() {
		return this.getAttribute("session");
	}
	get params() {
		return JSON.parse(this.getAttribute("params"));
	}
	get completed() {
		return this.getAttribute("completed") === "true";
	}

	/* state helper */
	get popupUrl() {
		return this.api.buildAuthFallbackUrl(
			this.stage,
			this.session,
			this.baseUrl
		);
	}
	get isKnownStage() {
		return KNOWN_AUTH_STAGES.has(this.stage);
	}
	get stageConfig() {
		return KNOWN_AUTH_STAGES.get(this.stage);
	}

	/* methods helpers */
	disableForm() {
		this.querySelector("fieldset")?.setAttribute("disabled", true);
	}

	checkAuthDone(authEvent) {
		const { data, origin } = authEvent;
		return data === HS_AUTH_DONE && origin === this.baseUrl;
	}

	/* events */
	async onWindowAuthMessage(authEvent) {
		// check it's the right message from the right place.
		if (!this.checkAuthDone(authEvent)) {
			return;
		} else {
			this.disableForm();
		}
		this.popupWindow.close();
		window.removeEventListener(ON_WINDOW_MESSAGE, this.onWindowAuthMessage);
		this.onStageCompleted(authEvent);
	}
	onStageStart(event) {
		event.stopPropagation();
		event.preventDefault();
		this.popupWindow = window.open(this.popupUrl, this.stage);
		window.addEventListener(
			ON_WINDOW_MESSAGE,
			this.onWindowAuthMessage.bind(this)
		);
	}
	onStageCompleted(event) {
		event.preventDefault();
		event.stopPropagation();
		this.dispatchEvent(
			new CustomEvent(ON_AUTH_STAGE, {
				bubbles: true,
				detail: {
					stage: this.stage,
					data: event.detail || event.data,
				},
			})
		);
	}

	/* lifecycle */
	attributeChangedCallback() {
		this.render();
	}
	constructor() {
		super();
		this.api = matrixApi;
	}
	connectedCallback() {
		this.render();
	}
	render() {
		if (this.stage && this.session) {
			const $stage = this.isKnownStage
				? this.createStage(this.stageConfig)
				: this.createUnkownStage(this.stage);
			this.replaceChildren($stage);
		} else {
			this.replaceChildren();
		}
	}
	createStage({ element, event } = {}) {
		if (element && event) {
			const $stage = document.createElement(element);
			$stage.setAttribute("base-url", this.baseUrl);
			$stage.addEventListener(event, this.onStageCompleted.bind(this));
			return $stage;
		} else {
			// for "dummy" event
			this.onStageCompleted(new CustomEvent(ON_AUTH_STAGE, { detail: true }));
		}
	}
	createUnkownStage(stage) {
		const $button = document.createElement("button");
		$button.setAttribute("type", "submit");
		$button.textContent = stage;

		const $form = document.createElement("form");
		$form.addEventListener("submit", this.onStageStart.bind(this));
		const $fieldset = document.createElement("fieldset");
		$fieldset.append($button);
		$form.append($fieldset);
		return $form;
	}
}
