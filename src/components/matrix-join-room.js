import MatrixApi from "../services/api.js";

// Renders a button that allows a guest user to join a room.
// The first time you tap the button, it will show you a consent link.
// After giving consent, tap the button again to really join the room.

export default class MatrixJoinRoom extends HTMLElement {
	static get observedAttributes() {
		return ["room-id", "consent", "is-member"];
	}
	get roomId() {
		return this.getAttribute("room-id");
	}
	get consent() {
		return JSON.parse(this.getAttribute("consent"));
	}
	set consent(obj) {
		if (obj) {
			this.setAttribute("consent", JSON.stringify(obj));
		} else {
			this.removeAttribute("consent");
		}
	}
	get isMember() {
		return this.getAttribute("is-member") === "true";
	}
	set isMember(bool) {
		if (bool) {
			this.setAttribute("is-member", bool);
		} else {
			this.removeAttribute("is-member");
		}
	}
	async attributeChangedCallback(name, newValue, oldValue) {
		if (newValue !== oldValue) {
			this.isMember = await this.getMembembershipStatus();
			this.render();
		}
	}
	async connectedCallback() {
		this.isMember = await this.getMembembershipStatus();
		this.render();
	}
	async getMembembershipStatus() {
		if (!this.roomId) return;
		try {
			/* not using getRoomJoinedMembers, because doest not work for guest user */
			const res = await MatrixApi.getRoomMembers(this.roomId);
			const { chunk: membershipEvents } = res;
			const { content } = MatrixApi.isUserJoinRoom(membershipEvents);
			if (content) {
				this.consent = null;
			}
			return !!content;
		} catch (e) {
			return false;
		}
	}
	async handleJoin(event) {
		event.preventDefault();
		const res = await MatrixApi.joinRoom(this.roomId);

		// Handle the state where user needs to give consent.
		if (res.errcode === "M_CONSENT_NOT_GIVEN") {
			this.consent = res;
		} else {
			this.isMember = true;
		}
		this.dispatchEvent(
			new CustomEvent("join", {
				bubbles: true,
				detail: res,
			})
		);
	}
	async handleLeave(event) {
		event.preventDefault();
		const res = await MatrixApi.leaveRoom(this.roomId);
		if (!res.errcode) {
			this.consent = null;
			this.isMember = false;
		}
		this.dispatchEvent(
			new CustomEvent("leave", {
				bubbles: true,
				detail: res,
			})
		);
	}
	render() {
		let $doms = [];
		if (this.consent) {
			$doms = [this.renderConsent(), this.renderJoin()];
		} else if (this.isMember) {
			$doms = [this.renderJoined()];
		} else {
			$doms = [this.renderJoin()];
		}
		this.innerHTML = "";
		this.append(...$doms);
	}
	renderJoin() {
		const $button = document.createElement("button");
		if (this.roomId) {
			$button.textContent = "Join room";
			$button.addEventListener("click", this.handleJoin.bind(this));
		} else {
			$button.textContent = "Missing room-id";
		}
		return $button;
	}
	renderConsent() {
		const $link = document.createElement("a");
		$link.href = this.consent.consent_uri;
		$link.innerText = this.consent.error;
		$link.target = "_blank";
		return $link;
	}
	renderJoined() {
		const $button = document.createElement("button");
		$button.textContent = "Leave room";
		$button.addEventListener("click", this.handleLeave.bind(this));
		return $button;
	}
}
