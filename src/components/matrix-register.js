import matrixApi from "../services/api.js";
import MX_DOM_EVENTS from "../services/dom-events.js";

const { ON_AUTH_FLOW, ON_REGISTER_AVAILABLE } = MX_DOM_EVENTS;
const DEFAULT_REGISTER_HOMESERVERS = [
	"matrix.org",
	"mozilla.org",
	"envs.net",
	"localhost",
];

export default class MatrixRegister extends HTMLElement {
	/* props */
	get username() {
		return this.getAttribute("username");
	}
	set username(str) {
		this.setAttribute("username", str);
	}
	get homeservers() {
		return (
			JSON.parse(this.getAttribute("homeservers")) ||
			DEFAULT_REGISTER_HOMESERVERS
		);
	}
	get homeserver() {
		return this.getAttribute("homeserver");
	}
	set homeserver(str) {
		this.setAttribute("homeserver", str);
	}
	get baseUrl() {
		return this.getAttribute("base-url");
	}
	set baseUrl(str) {
		this.setAttribute("base-url", str);
	}

	/* state */
	api = null;
	error = null;
	flowRes = null;
	registerRes = null;
	completedFlowStages = null;
	homeserverVersions = null;

	/* helpers */
	/* not sure how this helps; matrix UIA confused */
	get missingStage() {
		const [stage] = Array.from(this.completedFlowStages).find(
			([stage, stageData]) => {
				return !this.flowRes?.completed?.includes(stage);
			}
		);
		return stage;
	}

	/* events */
	async onUsername(event) {
		event.preventDefault();
		this.username = event.detail;
		await this.registerUser();
		this.render();
	}
	async onFlowCompleted(event) {
		this.completedFlowStages = event.detail;
		const request = this.api.buildFlowStageRequest({
			session: this.flowRes.session,
			username: this.username,
		});
		try {
			await this.registerUser(request);
		} catch (e) {
			console.info("Error duging register flow", e);
			throw e;
		}
		this.render();
	}
	async onRegisterSubmit(event) {
		event.preventDefault();
		event.stopPropagation();
		const formData = new FormData(event.target);
		let request;
		if (this.completedFlowStages && this.missingStage) {
			request = this.api.buildFlowStageRequest({
				stage: this.missingStage,
				stageData: this.completedFlowStages.get(this.missingStage),
				username: this.username,
				password: formData.get("password"),
				session: this.flowRes.session,
			});
		} else {
			request = {};
		}
		await this.registerUser(request);
		this.render();
	}
	async onHomeserver(event) {
		event.preventDefault();
		event.stopPropagation();
		const homeserverPrompt = event.target.value;
		try {
			if (true || this.homeservers.includes(homeserverPrompt)) {
				let discoverUrl = "";
				try {
					discoverUrl = new URL(homeserverPrompt).href;
				} catch (e) {
					discoverUrl = `https://${homeserverPrompt}`;
				}

				const { ["m.homeserver"]: { base_url } = {} } =
					await this.api.getWellKnown(discoverUrl);
				const homeserverVersions = await this.api.getVersions(base_url);
				if (homeserverVersions.error) {
					throw homeserverVersions;
				}
				this.homeserverVersions = homeserverVersions;
				this.homeserver = new URL(discoverUrl).host;
				this.baseUrl = base_url;
				this.render();
			}
		} catch (notHomeserverError) {
			console.info("Not a valid homeserver", notHomeserverError);
		}
	}

	/* methods */
	async registerUser(userRequest = {}) {
		this.error = null;
		try {
			/* get the original flows without user request or kind (raw /register) */
			const res = !Object.entries(userRequest).length
				? await this.api.registerUser({}, this.baseUrl)
				: await this.api.registerUser(userRequest, this.baseUrl);
			if (res.user_id) {
				this.registerRes = res;
				this.flowRes = null;
			} else {
				this.flowRes = res;
			}
		} catch (error) {
			console.info("Error durring registration", error);
			this.error = error;
		}
	}

	/* lifecycle */
	constructor() {
		super();
		this.api = matrixApi;
	}
	async connectedCallback() {
		this.render();
	}
	render() {
		const $doms = [];
		if (this.registerRes) {
			$doms.push(this.createRegisterSuccess(this.registerRes));
		} else if (this.completedFlowStages) {
			$doms.push(this.createFormUser(this.username, this.homeserver));
		} else if (!this.homeserver && this.homeservers) {
			$doms.push(
				this.createHomeserverSelect(this.homeservers, this.homeserver)
			);
		} else if (!this.username && this.homeserver) {
			$doms.push(this.createUsernameAvailable(this.username, this.homeserver));
		} else if (this.flowRes && this.baseUrl) {
			$doms.push(this.createAuthFlow(this.flowRes, this.baseUrl));
		}
		if (this.error) {
			$doms.push(this.createError(this.error));
		}
		if (this.flowRes?.error) {
			$doms.push(this.createError(this.flowRes));
		}
		this.replaceChildren(...$doms);
	}
	createError(error) {
		const $error = document.createElement("matrix-error");
		$error.setAttribute("error", JSON.stringify(error));
		return $error;
	}
	createFormUser(username, homeserver) {
		const $form = document.createElement("form");
		$form.addEventListener("submit", this.onRegisterSubmit.bind(this));
		$form.append(
			this.createUsernameField(username),
			this.createPasswordField(),
			this.createSubmitField()
		);
		return $form;
	}
	createHomeserverSelect(homeservers, homeserver) {
		const $fieldset = document.createElement("fieldset");
		const $legend = document.createElement("legend");
		$legend.textContent = "Homeserver";

		const $input = document.createElement("input");
		$input.setAttribute("list", "homeserver-list");
		$input.name = "homeserver";

		const $datalist = document.createElement("datalist");
		$datalist.id = "homeserver-list";

		homeservers.forEach((hs) => {
			const $option = document.createElement("option");
			$option.value = hs;
			$datalist.appendChild($option);
		});

		$input.value = homeserver;
		$input.addEventListener("input", this.onHomeserver.bind(this));

		$fieldset.append($legend, $input, $datalist);
		return $fieldset;
	}
	createUsernameAvailable(username, homeserver) {
		const $u = document.createElement("matrix-register-username-available");
		$u.addEventListener(ON_REGISTER_AVAILABLE, this.onUsername.bind(this));
		username && $u.setAttribute("username", username);
		homeserver && $u.setAttribute("homeserver", homeserver);
		return $u;
	}
	createPasswordField() {
		const $passwordField = document.createElement("fieldset");
		const $passwordLegend = document.createElement("legend");
		$passwordLegend.textContent = "User password";
		const $password = document.createElement("input");
		$password.type = "password";
		$password.name = "password";
		$password.placeholder = "password (ex: generate four random words)";
		$password.required = true;
		$passwordField.append($passwordLegend, $password);
		return $passwordField;
	}
	createUsernameField(username) {
		const $usernameField = document.createElement("fieldset");
		const $usernameLegend = document.createElement("legend");
		$usernameLegend.textContent = "Username";
		const $username = document.createElement("input");
		$username.type = "text";
		$username.name = "username";
		$username.placeholder = "username";
		$username.required = true;
		if (username) {
			$username.value = username;
		}
		$usernameField.append($usernameLegend, $username);
		return $usernameField;
	}
	createSubmitField() {
		const $submitField = document.createElement("fieldset");
		const $submit = document.createElement("button");
		$submit.textContent = "Register new matrix user";
		$submit.type = "submit";
		$submitField.append($submit);
		return $submitField;
	}
	createAuthFlow(registerFlowRes, baseUrl) {
		const { flows, params, session, completed } = registerFlowRes;
		const $flow = document.createElement("matrix-auth-flows");
		$flow.addEventListener(ON_AUTH_FLOW, this.onFlowCompleted.bind(this));
		baseUrl && $flow.setAttribute("base-url", baseUrl);
		flows && $flow.setAttribute("flows", JSON.stringify(flows));
		params && $flow.setAttribute("params", JSON.stringify(params));
		completed && $flow.setAttribute("completed", JSON.stringify(completed));
		$flow.setAttribute("session", session);
		return $flow;
	}
	createRegisterSuccess(registerRes) {
		const { user_id } = registerRes;
		const $success = document.createElement("code");
		$success.textContent = user_id;
		return $success;
	}
}
