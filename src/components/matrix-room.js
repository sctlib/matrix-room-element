import mxApi from "../services/api.js";
import MX_DOM_EVENTS from "../services/dom-events.js";
const { ON_READY, ON_CONTEXT, ON_ROOM_ACTION } = MX_DOM_EVENTS;

const template = document.createElement("template");
template.innerHTML = `
	<slot name="state"></slot>
	<slot name="send"></slot>
	<slot name="widgets"></slot>
	<slot name="events"></slot>
	<slot name="dialog">
		<wcu-dialog>
			<span slot="open"></span>
			<aside slot="dialog"></aside>
		</wcu-dialog>
	</slot>
`;

export default class MatrixRoom extends HTMLElement {
	static get observedAttributes() {
		return [
			"origin",
			"filter",
			"show-event-info",
			"show-event-sender",
			"show-event-actions",
			"show-send-event",
			"send-event-types",
			"show-space-children",
			"show-space-parents",
			"show-context",
			"context-limit",
			"profile-id",
			"event-id",
			"events",
			"room-info",
			"is-user-join-room",
		];
	}

	/* set from outside */
	get profileId() {
		return this.getAttribute("profile-id");
	}
	get eventId() {
		return this.getAttribute("event-id");
	}
	set eventId(str) {
		this.setAttribute("event-id", str);
	}
	get origin() {
		return this.getAttribute("origin");
	}
	get flagSanitizer() {
		/* not an observed attribute, because should not change after first set */
		return this.getAttribute("flag-sanitizer") === "true";
	}
	get hideEvents() {
		/* use when nesting room in room, sub-spaces */
		return this.getAttribute("hide-events") === "true";
	}
	get hideWidgets() {
		return this.getAttribute("hide-widgets") === "true";
	}
	get showEventInfo() {
		return this.getAttribute("show-event-info") === "true";
	}
	get showEventSender() {
		return this.getAttribute("show-event-sender") === "true";
	}
	get showEventActions() {
		return this.getAttribute("show-event-actions") === "true";
	}
	get showRoomActions() {
		return this.getAttribute("show-room-actions") === "true";
	}
	get showSendEvent() {
		return this.getAttribute("show-send-event") === "true";
	}
	get showContext() {
		return this.getAttribute("show-context") === "true";
	}
	get showSpaceParents() {
		return this.getAttribute("show-space-parents") === "true";
	}
	get showSpaceChildren() {
		return this.getAttribute("show-space-children") === "true";
	}
	get contextLimit() {
		const max = 100;
		const limit = Number(this.getAttribute("context-limit")) || max;
		return limit > max ? max : limit;
	}
	get sendEventTypes() {
		return (
			JSON.parse(this.getAttribute("send-event-types")) || ["m.room.message"]
		);
	}
	get filter() {
		return (
			JSON.parse(this.getAttribute("filter")) || {
				types: ["m.room.message", "org.libli.room.message.track"],
				not_types: [
					"m.room.redaction",
					"m.room.member",
					"libli.widget",
					"im.vector.modular.widgets",
					"im.vector.modular.widgets.libli.widget",
					"m.reaction",
					"m.room.history_visibility",
					"m.room.power_levels",
					"m.room.name",
					"m.room.topic",
					"m.space.child",
					"m.room.avatar",
				],
			}
		);
	}
	set filter(obj) {
		this.setAttribute("filter", JSON.stringify(obj));
	}
	/* set from inside */
	get roomInfo() {
		return JSON.parse(this.getAttribute("room-info"));
	}
	set roomInfo(obj) {
		if (!obj) {
			this.removeAttribute("is-user-join-room");
			this.removeAttribute("room-info");
		} else {
			const joinedRoom = !!this.api.isUserJoinRoom(obj?.members || []);
			this.setAttribute("is-user-join-room", joinedRoom);
			this.setAttribute("room-info", JSON.stringify(obj));
		}
	}

	get isUserJoinRoom() {
		return this.getAttribute("is-user-join-room") === "true";
	}

	/* dom helpers */
	get $state() {
		return this.querySelector('slot[name="state"]');
	}
	get $widgets() {
		return this.querySelector('slot[name="widgets"]');
	}
	get $events() {
		return this.querySelector('slot[name="events"]');
	}
	get $send() {
		return this.querySelector('slot[name="send"]');
	}
	get $dialog() {
		return this.querySelector('slot[name="dialog"] wcu-dialog');
	}
	get $dialogContent() {
		return this.querySelector(
			'slot[name="dialog"] wcu-dialog [slot="dialog"]'
		);
	}

	/* re-render the contexts */
	attributeChangedCallback(attrName, oldValue, newValue) {
		if (attrName === "event-id" && oldValue) {
			/* clean events to render new one */
			this.$events.innerHTML = "";
			this.renderEventContext();
		}
		if (attrName === "event-types") {
			this.renderSendEvent();
		}
	}
	async connectedCallback() {
		this.api = mxApi;
		try {
			if (this.profileId) {
				const profile = await this.api.checkMatrixId(this.profileId);
				if (profile) {
					const roomRes = await this.api.getRoomInfo(profile);
					if (roomRes.error) {
						throw roomRes;
					}
					this.roomInfo = roomRes;
				}
			}
		} catch (error) {
			this.roomError = error;
			console.error("Error fetching profile", error, this.profile);
		}
		await this.init();
		this.dispatchEvent(
			new CustomEvent(ON_READY, {
				bubbles: true,
				detail: this.roomInfo || this.roomError,
			})
		);
	}
	disconnectedCallback() {
		if (this.syncAbortController) {
			this.syncAbortController.abort();
		}
	}
	async init() {
		/* abort previous sync, if any */
		if (this.syncAbortController) {
			this.syncAbortController.abort();
		}
		this.replaceChildren(template.content.cloneNode(true));

		/* 1. room info, then render it */
		if (this.roomInfo) {
			this.renderInfo(this.roomInfo);
			if (this.shouldRenderSendEvent()) {
				this.renderSendEvent();
			}
		} else if (this.roomError) {
			this.renderError(this.roomError);
		}

		/* 2. get the context of the room/event:
			 - always show when only an event (but not "load more" buttons)
			 - if we're displaying the whole room, show events if show-context,
			 and then also render the buttons */
		if (this.eventId && this.roomInfo?.state) {
			this.renderEventContext();
		} else if (this.showContext && !this.roomInfo?.spaceChildren?.length) {
			if (this.roomInfo?.lastEvent) {
				/* returns a first /sync, otherwise /$room_id/messages */
				try {
					const sync = await this.renderRoomContext();
					sync && (await this.synchronise(sync));
				} catch (e) {
					console.error("Error syncing", e);
				}
			}
		}
	}

	filterWidgets(widget) {
		if (widget.type !== "im.vector.modular.widgets") return;
		if (!widget.content) return;
		/* etherpad provided by element/vector.im cannot be iframed? (cors) */
		if (widget.content.type === "etherpad") return;
		/* we know how to handle only youtube iframe */
		if (widget.content.type === "youtube") return true;
	}
	/* sort events in the right after/before timeline order (newest top) */
	sortEvents(e_before, e_after) {
		return e_after.origin_server_ts - e_before.origin_server_ts;
	}
	async onRoomAction({ detail }) {
		const {
			target: { value },
		} = detail;

		if (value === "join" && this.roomInfo.state.id) {
			const res = await this.api.joinRoom(this.roomInfo.state.id);
			if (res.errcode === "M_CONSENT_NOT_GIVEN" && res.consent_uri) {
				this.renderConsent(res);
			}
			if (res.room_id) {
				this.init();
			}
		} else if (value === "leave" && this.roomInfo.state.id) {
			const res = await this.api.leaveRoom(this.roomInfo.state.id);
			this.init();
		} else if (value === "forget" && this.roomInfo.state.id) {
			const res = await this.api.forgetRoom(this.roomInfo.state.id);
			this.init();
		}
	}
	async onLoadMoreContext({ detail }) {
		/* if the is an event to load more context from */
		if (detail.event) {
			const context = await this.api.getRoomEventContext({
				roomId: this.roomInfo.state.id,
				eventId: detail.event.event_id,
				params: [
					["filter", this.filter],
					["limit", this.contextLimit],
				],
			});
			/* this event cannot have the direction "initial",
				 as only the initial context/event has it, and we're here
				 requesting new context (possibly, from initial, but after/before it) */
			if (detail.dir === "after" && context.events_after) {
				this.renderContext({
					dir: detail.dir,
					showContext: this.showContext,
					events: context.events_after,
				});
			} else if (detail.dir === "before" && context.events_before) {
				this.renderContext({
					dir: detail.dir,
					showContext: this.showContext,
					events: context.events_before,
				});
			}
		}
	}
	async synchronise(previousSync) {
		if (previousSync.errcode) return;
		const filter = {
			room: {
				rooms: [this.roomInfo.state.id],
				timeline: this.filter,
			},
		};
		const params = [
			["filter", filter],
			["limit", this.contextLimit],
		];
		if (previousSync && previousSync.next_batch) {
			params.push(["since", previousSync.next_batch]);
		}

		// setup AbortController
		this.syncAbortController = new AbortController();
		// signal to pass to fetch
		const signal = this.syncAbortController.signal;

		let sync;
		try {
			sync = await this.api.sync({
				params,
				signal,
			});
			if (!sync.next_batch) {
				throw sync;
			}
		} catch (error) {
			console.error("Error syncing room", this.roomInfo.state.id, error);
			return;
		}

		/* if there are rooms, there is a result for our query */
		const { rooms } = sync;
		if (rooms) {
			const {
				timeline: { events },
			} = rooms.join[this.roomInfo.state.id];
			this.renderContext({
				dir: "after",
				showContext: false,
				events: events,
			});
		}

		/* restart synchronise,
			 "long-polling" (wait server response for new events) */
		this.synchronise(sync);
	}
	async renderEventContext(params = {}) {
		const { eventId = this.eventId, dir = "initial" } = params;

		/* when rendering just one event, clean events slot */
		const eventContext = await this.api.getRoomEventContext({
			roomId: this.roomInfo.state.id,
			eventId: eventId,
			params: [
				["filter", this.filter],
				["limit", this.contextLimit],
			],
		});
		if (eventContext) {
			this.renderContext({
				dir: dir,
				events: [eventContext.event],
				showContext: this.showContext,
			});
		}
	}
	shouldRenderSendEvent() {
		if (!this.showSendEvent || this.roomInfo?.spaceChildren?.length > 0) {
			return false;
		}

		if (this.api.auth && this.api.user.access_token && this.isUserJoinRoom) {
			return true;
		}
		if (
			this.api.authGuest &&
			this.api.guestUser.access_token &&
			this.isUserJoinRoom
		) {
			return true;
		}
		return false;
	}
	async renderRoomContext() {
		const filter = {
			room: {
				rooms: [this.roomInfo.state.id],
				timeline: this.filter,
			},
		};
		const sync = await this.api.sync({
			roomId: this.roomInfo.state.id,
			params: [
				["filter", filter],
				["limit", this.contextLimit],
				["since", 0],
			],
		});
		/* if we have data for the room, there are timeline events */
		const { rooms } = sync;
		if (rooms) {
			const {
				timeline: { events },
			} = rooms.join[this.roomInfo.state.id];

			if (events.length) {
				this.renderContext({
					events: events,
					dir: "initial",
					showContext: this.showContext,
				});
			}
			/* return sync for the next_batch (for next sync) */
			return sync;
		} else {
			/* if the user has not joined, the data is not in the object "sync.rooms.join",
				 therefore let's try and "just query the message for the room " */
			const lastPage = await this.api.getRoomMessages({
				roomId: this.roomInfo.state.id,
				params: [
					["filter", this.filter],
					["dir", "b"],
					["limit", this.contextLimit],
				],
			});
			/* if there are message events, get the context of the last one */
			if (lastPage && lastPage.chunk && lastPage.chunk.length) {
				this.renderContext({
					events: lastPage.chunk,
					dir: "initial",
					showContext: this.showContext,
				});
			}
			return lastPage;
		}
	}
	renderInfo(roomInfo) {
		roomInfo?.state && this.renderState(roomInfo.state);
		roomInfo?.widgets && this.renderWidgets(roomInfo.widgets);
	}
	renderError(error) {
		const $error = document.createElement("matrix-error");
		$error.setAttribute("error", JSON.stringify(error));
		this.$state.append($error);
	}
	renderSendEvent() {
		const $sendEvents = document.createElement("matrix-send-events");
		$sendEvents.setAttribute(
			"event-types",
			JSON.stringify(this.sendEventTypes)
		);
		$sendEvents.setAttribute("profile-id", this.roomInfo.state.id);
		this.$send.innerHTML = "";
		this.$send.append($sendEvents);
	}
	renderState(state = {}) {
		if (Object.keys(this.roomInfo.state).length > 0) {
			const elName = "matrix-room-state";
			const $roomState =
				this.$state.querySelector(elName) || document.createElement(elName);
			$roomState.setAttribute("state", JSON.stringify(this.roomInfo.state));
			this.origin && $roomState.setAttribute("origin", this.origin);
			this.roomInfo.widgets &&
				$roomState.setAttribute(
					"widgets",
					JSON.stringify(this.roomInfo.widgets)
				);

			if (this.showSpaceChildren && this.roomInfo.spaceChildren) {
				$roomState.setAttribute(
					"space-children",
					JSON.stringify(this.roomInfo.spaceChildren)
				);
			}
			if (this.showSpaceParents && this.roomInfo.spaceParents) {
				$roomState.setAttribute(
					"space-parents",
					JSON.stringify(this.roomInfo.spaceParents)
				);
			}
			if (this.flagSanitizer && typeof Sanitizer === "function") {
				$roomState.setAttribute("flag-sanitizer", true);
			}
			this.$state.append($roomState);
		} else {
			this.setAttribute("disabled", true);
		}

		if (this.showRoomActions) {
			const $roomActions = document.createElement("matrix-room-actions");
			$roomActions.addEventListener(
				ON_ROOM_ACTION,
				this.onRoomAction.bind(this)
			);
			$roomActions.setAttribute("is-user-join-room", this.isUserJoinRoom);
			this.$state.append($roomActions);
		}
	}

	renderConsent(error) {
		const consentTemplate = `
			<h2>Matrix services consent agreement</h2>
			<p>${error.error}</p>
			<p>
				<a href="${error.consent_uri}" target="_blank" rel="nofollow noreferrer">Matrix consent agreement</a>
			</p>
			<p>
				When you are done, try your previously attempted action again.
			</p>
		`;
		this.renderDialog(consentTemplate);
	}
	renderDialog(htmlString) {
		this.$dialogContent.innerHTML = htmlString || "";
		this.$dialog.open();
	}
	renderWidgets() {
		if (this.roomInfo.widgets && this.origin && !this.hideWidgets) {
			const knownWidgets = this.roomInfo.widgets.filter(this.filterWidgets);
			if (knownWidgets.length) {
				const $roomWidgets = document.createElement("matrix-room-widgets");
				$roomWidgets.setAttribute("widgets", JSON.stringify(knownWidgets));
				$roomWidgets.setAttribute("origin", this.origin);
				this.$widgets.append($roomWidgets);
			}
		}
	}

	/* render a context block,
		 from a specified event-id, in specific direction */
	renderContext({
		events = [],
		dir = "after", // or 'before' or 'initial'
		showContext = false,
	}) {
		if (!events.length) return;

		const $ctx = document.createElement("matrix-room-context");
		$ctx.addEventListener("loadcontext", this.onLoadMoreContext.bind(this));
		$ctx.setAttribute("events", JSON.stringify(events.sort(this.sortEvents)));
		$ctx.setAttribute("dir", dir);
		$ctx.setAttribute("profile-id", this.profileId);
		if (showContext) {
			$ctx.setAttribute("show-context", showContext);
		}
		if (this.origin) {
			$ctx.setAttribute("origin", this.origin);
		}
		if (this.showEventInfo) {
			$ctx.setAttribute("show-info", true);
		}
		if (this.showEventSender) {
			$ctx.setAttribute("show-sender", true);
		}
		if (this.showEventActions) {
			$ctx.setAttribute("show-actions", true);
		}
		if (this.flagSanitizer) {
			$ctx.setAttribute("flag-sanitizer", true);
		}

		/* depending on "where" we add (and "for when", which context"-of-what"):
		 - "before in time" (so above any existing context)
		 - "after in time" (so under any existing context)
			 Natural sort is newest at the top */
		if (dir === "before") {
			this.$events.append($ctx);
		} else if (dir === "after") {
			this.$events.prepend($ctx);
		} else {
			/* here it does not matter; it is "the first context" (empty container)*/
			this.$events.prepend($ctx);
		}

		/* send an event  */
		const eventContext = new CustomEvent(ON_CONTEXT, {
			bubbles: true,
			detail: {
				events,
				dir,
			},
		});
		this.dispatchEvent(eventContext);
	}
}
