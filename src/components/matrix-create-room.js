import MatrixApi from "../services/api.js";
import MX_DOM_EVENTS from "../services/dom-events.js";
const { ON_ROOM_CREATE } = MX_DOM_EVENTS;

const template = document.createElement("template");
template.innerHTML = `
	<form>
		<slot name="roomInfo"></slot>
		<fieldset>
			<button type="submit">create room</button>
		</fieldset>
	</form>
`;

export default class MatrixCreateRoom extends HTMLElement {
	/* the state of the form */
	state = {
		name: "",
		topic: "",
		alias: "",
	};
	connectedCallback() {
		this.append(template.content.cloneNode(true));
		this.$form = this.querySelector("form");
		this.$form.addEventListener("submit", this.handleSubmit.bind(this));
		this.$fieldsets = this.querySelectorAll("fieldset");
		this.$roomInfo = this.querySelector('slot[name="roomInfo"');
		this.render();
	}
	handleInput({ target }) {
		const { name, value } = target;
		this.state = {
			...this.state,
			[name]: value,
		};
	}
	render() {
		const $name = document.createElement("input");
		$name.addEventListener("input", this.handleInput.bind(this));
		$name.type = "text";
		$name.name = "name";
		$name.value = this.state.name;
		$name.placeholder = "Name";

		const $alias = document.createElement("input");
		$alias.addEventListener("input", this.handleInput.bind(this));
		$alias.type = "text";
		$alias.name = "alias";
		$alias.value = this.state.alias;
		$alias.placeholder = "Alias (#alias:matrix.org)";

		const $topic = document.createElement("textarea");
		$topic.addEventListener("input", this.handleInput.bind(this));
		$topic.name = "topic";
		$topic.value = this.state.topic;
		$topic.placeholder = "Topic";

		const $els = [$name, $alias, $topic];
		$els.forEach(($el) => {
			const $fieldset = document.createElement("fieldset");
			$fieldset.append($el, this.createFormOutput($el))
			this.$roomInfo.append($fieldset);
		});
	}
	createFormOutput($el) {
		const $output = document.createElement("output");
		$output.setAttribute("for", $el.name);
		$output.value = "";
		return $output;
	}
	async handleSubmit(event) {
		event.preventDefault();
		this.disableForm();

		let res;
		try {
			res = await MatrixApi.createRoom({
				name: this.state.name,
				topic: this.state.topic,
				alias: this.state.alias,
			});
			if (res.errcode) {
				throw res;
			}
			/* send JS event if no error */
			const newDOMEvent = new CustomEvent(ON_ROOM_CREATE, {
				bubbles: true,
				detail: res,
			});
			this.dispatchEvent(newDOMEvent);
			this.resetForm();
		} catch (error) {
			console.log("Error creating room", error);
			this.handleSubmitError(error);
		}
		this.enableForm();
	}
	handleSubmitError({ errcode, error }) {
		/* reset all existing outputs */
		this.$form.querySelectorAll("output").forEach(($out) => {
			$out.innerText = "";
		});
		/* set errors on outputs */
		if (errcode === "M_ROOM_IN_USE") {
			const $o = this.$form.querySelector('output[for="alias"]');
			$o.innerText = error;
		}
	}
	disableForm() {
		this.$fieldsets.forEach(($fieldset) =>
			$fieldset.setAttribute("disabled", true)
		);
		this.setAttribute("loading", true);
	}
	enableForm() {
		this.$fieldsets.forEach(($fieldset) =>
			$fieldset.removeAttribute("disabled")
		);
		this.removeAttribute("loading");
	}
	resetForm() {
		if (this.$form) {
			this.$form.reset();
		}
	}
}
