import MatrixApi from "../services/api.js";
import MX_DOM_EVENTS from "../services/dom-events.js";
const { ON_ROOM_ACTION } = MX_DOM_EVENTS;
/*
	 Actions regarding an event;
	 differs if user is (un)authed, sender (owner) of the event etc.
	 Not supposed to be used independently, outside of <matrix-event/>
 */
export default class MatrixRoomActions extends HTMLElement {
	static get observedAttributes() {
		return ["room-id", "is-user-join-room"];
	}
	get roomId() {
		return this.getAttribute("room-id");
	}

	get defaultText() {
		return this.getAttribute("room-id") || "...";
	}

	get isUserJoinRoom() {
		return this.getAttribute("is-user-join-room") === "true";
	}

	async connectedCallback() {
		this.$form = document.createElement("form");
		this.$select = document.createElement("select");
		this.$select.addEventListener("input", this.onInput.bind(this));

		this.$form.append(this.$select);
		this.append(this.$form);
		this.render();
	}
	disconnectedCallback() {
		this.$root && this.$root.removeEventListener("change", this.onInput);
	}
	render() {
		const $default = document.createElement("option");
		$default.innerText = this.defaultText;
		$default.value = "default";
		$default.setAttribute("disabled", true);
		$default.setAttribute("selected", true);

		const $toggleJoin = document.createElement("option");
		if (this.isUserJoinRoom) {
			$toggleJoin.innerText = "Leave";
			$toggleJoin.value = "leave";
		} else {
			if (MatrixApi.userAny) {
				$toggleJoin.innerText = "Join";
			} else {
				$toggleJoin.innerText = "Login to join";
			}
			$toggleJoin.value = "join";
		}

		this.$select.append($default);
		this.$select.append($toggleJoin);
	}

	onInput(event) {
		const {
			target: { value },
		} = event;

		if (value === "share") {
			this.renderDialogShare();
		}
		this.dispatchEvent(
			new CustomEvent(ON_ROOM_ACTION, {
				bubbles: true,
				detail: event,
			})
		);
		this.resetValue();
	}
	resetValue() {
		this.$select.selectedIndex = 0;
	}
}
