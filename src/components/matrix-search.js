import matrixApi from "../services/api.js";
import MX_DOM_EVENTS from "../services/dom-events.js";
const { ON_SEARCH } = MX_DOM_EVENTS;

/*
	 not all room aliases are present;
	 https://spec.matrix.org/v1.9/client-server-api/#room-aliases */

const findEventMemberUser = (event) => {
	return [event.user_id];
};
const findEventRoomAliasRooms = (event) => {
	const { alias = "", alt_aliases = [] } = event.content;
	return [alias, ...alt_aliases];
};
const reduceEventRoomMember = (acc, memberEvent) => {
	acc.push(...findEventMemberUser(memberEvent));
	return acc;
};
const reduceEventRoomAlias = (acc, aliasEvent = {}) => {
	acc.push(...findEventRoomAliasRooms(aliasEvent));
	return acc;
};

const getDataFromJoinedRooms = (joinedRooms = []) => {
	const roomData = joinedRooms?.reduce((acc, joinedRoom) => {
		const { eventsByTypes } = joinedRoom;
		const roomAliasEvents = eventsByTypes["m.room.canonical_alias"] || [];
		const roomMemberEvents = eventsByTypes["m.room.member"] || [];
		const roomAliases = roomAliasEvents.reduce(reduceEventRoomAlias, []);
		const roomMembers = roomMemberEvents.reduce(reduceEventRoomMember, []);
		return [...acc, ...roomMembers, ...roomAliases];
	}, []);
	const roomMap = new Map();
	roomData
		?.filter((data) => !!data)
		.forEach((data) => {
			roomMap.set(data, true);
		});
	return roomMap;
};
const getDataFromRoomEvents = (roomEvents = []) => {
	const roomData = roomEvents?.reduce((acc, event) => {
		const { type } = event || {};
		if (type === "m.room.canonical_alias") {
			acc.push(...findEventRoomAliasRooms(event));
		} else if (type === "m.room.member") {
			acc.push(...findEventMemberUser(event));
		}
		return acc;
	}, []);
	const roomMap = new Map();
	roomData
		?.filter((data) => !!data)
		.forEach((data) => {
			roomMap.set(data, true);
		});
	return roomMap;
};

export default class MatrixSearch extends HTMLElement {
	/* props */
	get origin() {
		return this.getAttribute("origin");
	}
	get placeholder() {
		return (
			this.getAttribute("placeholder") ||
			"Search matrix (events in joined room)"
		);
	}
	get legend() {
		return this.getAttribute("legend") || "Search rooms";
	}
	get searchConfig() {
		return (
			JSON.parse(this.getAttribute("search-config")) || {
				search_categories: {
					[this.searchCategoryKey]: {
						[this.searchTermKey]: "",
					},
				},
			}
		);
	}
	get apisearch() {
		return this.getAttribute("apisearch") === "true";
	}
	get quicksearch() {
		return this.getAttribute("quicksearch") === "true";
	}
	get quicksearchEvents() {
		return JSON.parse(this.getAttribute("quicksearch-events"));
	}

	/* not yet props (SPEC) */
	get searchCategoryKey() {
		return "room_events";
	}
	get searchTermKey() {
		return "search_term";
	}
	/* dom utils */
	get $form() {
		return this.querySelector("form");
	}
	get $fieldsets() {
		return this.querySelectorAll("fieldset");
	}
	get $submitOutput() {
		return this.querySelector("output");
	}
	get $results() {
		return this.querySelector("matrix-search-results");
	}

	/* events */
	async onSubmit(event) {
		event.preventDefault();
		event.stopPropagation();
		delete this.results;
		const formData = new FormData(event.target);
		this.disableForm();
		const search_term = formData.get(this.searchTermKey);
		const config = this.getSearchConfig(search_term);
		if (this.apisearch && search_term && config) {
			try {
				const res = await matrixApi.search(config);
				this.results = res;
				if (res.error) {
					throw res;
				}
			} catch (error) {
				this.renderError(error);
			}
		}
		this.dispatchEvent(
			new CustomEvent(ON_SEARCH, {
				bubbles: true,
				detail: {
					config,
					[this.searchTermKey]: search_term,
					results: this.results,
				},
			})
		);
		this.renderResults(this.results);
		this.enableForm();
		return false;
	}
	onInput({ target: { name, value } }) {
		if (name === this.searchTermKey && !value) {
			delete this.results;
			this.renderResults();
		}
	}
	async onAuth({ detail }) {
		if (this.quicksearch) {
			this.quicksearchData = await this.getQuicksearchData();
		}
		this.render();
	}

	async getJoinedRooms() {
		const { joined_rooms: roomIds } = await matrixApi.getJoinedRooms();
		if (roomIds) {
			const roomPromises = roomIds.map((roomId) => {
				const profile = matrixApi.checkMatrixId(roomId);
				return matrixApi.getRoomInfo(profile);
			});
			return await Promise.all(roomPromises);
		}
	}

	/* form state */
	resetForm() {
		if (this.$form) {
			this.$form.reset();
			this.$submitOutput.replaceChildren();
		}
		delete this.results;
	}
	enableForm() {
		this.$fieldsets.forEach(($fieldset) => {
			$fieldset.removeAttribute("disabled");
		});
	}
	disableForm() {
		this.$fieldsets.forEach(($fieldset) => {
			$fieldset.setAttribute("disabled", true);
		});
	}

	/* methods */
	getSearchConfig(search_term) {
		const config = { ...this.searchConfig };
		const category = config.search_categories[this.searchCategoryKey];
		category[this.searchTermKey] = search_term;
		return config;
	}

	async getQuicksearchData() {
		let data;
		try {
			if (!this.quicksearchEvents?.length) {
				const joinedRooms = await this.getJoinedRooms();
				if (joinedRooms) {
					data = getDataFromJoinedRooms(joinedRooms);
				}
			} else if (this.quicksearchEvents?.length) {
				data = getDataFromRoomEvents(this.quicksearchEvents);
			}
		} catch (error) {
			console.info("Error quick search joined rooms", error);
		}
		return data;
	}

	/* lyfecycle */
	constructor() {
		super();
		/* because we want each list to be unique in their DOM id JS representation */
		this.dataListKey = `mwc-search-${self.crypto.randomUUID()}`;
	}
	async connectedCallback() {
		matrixApi.addEventListener("auth", this.onAuth.bind(this));
		if (this.quicksearch) {
			this.quicksearchData = await this.getQuicksearchData();
		}
		this.render();
	}
	disconnectedCallback() {
		matrixApi.removeEventListener("auth", this.onAuth);
		if (this.$form) {
			this.$form.removeEventListener("submit", this.onSubmit);
		}
	}

	/* render DOM */
	render() {
		this.replaceChildren();
		const $form = this.createForm();
		this.append($form);
	}
	renderError(error) {
		const $error = document.createElement("matrix-error");
		$error.setAttribute("error", JSON.stringify(error));
		this.$submitOutput.replaceChildren($error);
	}
	renderResults(results) {
		/* if quicksearch render en empty dom */
		const $results = this.createResults(results);
		if (this.$results) {
			this.$results.replaceWith($results);
		} else {
			this.append($results);
		}
	}

	createForm() {
		const $form = document.createElement("form");
		$form.addEventListener("submit", this.onSubmit.bind(this));
		$form.addEventListener("input", this.onInput.bind(this));

		const $searchField = document.createElement("fieldset");
		const $searchLegend = document.createElement("legend");
		$searchLegend.innerText = this.legend;
		const $searchInput = document.createElement("input");
		$searchInput.setAttribute("type", "search");
		$searchInput.setAttribute("name", this.searchTermKey);
		$searchInput.setAttribute("placeholder", this.placeholder);
		if (this.quicksearchData?.size) {
			$searchInput.setAttribute("list", this.dataListKey);
			const $searchData = document.createElement("datalist");
			$searchData.setAttribute("id", this.dataListKey);
			const $searchDataOptions = Array.from(this.quicksearchData).map(
				([option]) => {
					const $searchDataOption = document.createElement("option");
					$searchDataOption.setAttribute("value", option);
					return $searchDataOption;
				}
			);
			$searchData.append(...$searchDataOptions);
			$searchField.append($searchData);
		}
		$searchField.append($searchLegend, $searchInput);

		const $submitField = document.createElement("fieldset");
		const $submitOutput = document.createElement("output");
		const $submit = document.createElement("button");
		$submit.setAttribute("type", "submit");
		$submit.innerText = "Search";
		$submitField.append($submit, $submitOutput);

		$form.append($searchField, $submitField);
		return $form;
	}
	createResults({ search_categories } = {}) {
		if (!search_categories) {
			return "";
		}
		const $categories = Object.entries(search_categories).map(
			this.createResultsCategory.bind(this)
		);
		const $resultsContainer = document.createElement("matrix-search-results");
		if ($categories?.length) {
			$resultsContainer.append(...$categories);
		} else {
			const $noResults = document.createElement("matrix-search-noresults");
			$noResults.textContent = "No result for this search.";
			$resultsContainer.append($noResults);
		}
		return $resultsContainer;
	}
	createResultsCategory([categoryName, categoryData]) {
		const { count, results, highlights } = categoryData;

		const $category = document.createElement("matrix-search-category");
		$category.setAttribute("category", categoryName);
		const $count = document.createElement("matrix-search-count");
		const $countNum = document.createElement("code");
		$countNum.textContent = count;
		$count.append($countNum, " ", "results");
		$category.append($count);

		const $results = document.createElement("ol");
		const $categoryResults = results?.map((searchResult, index) => {
			const $result = document.createElement("li");
			const $resultRoomEvent = document.createElement("matrix-room");
			$resultRoomEvent.setAttribute("profile-id", searchResult.result.room_id);
			$resultRoomEvent.setAttribute("event-id", searchResult.result.event_id);
			$resultRoomEvent.setAttribute("show-event-info", true);
			this.origin && $resultRoomEvent.setAttribute("origin", this.origin);
			$result.append($resultRoomEvent);
			return $result;
		});
		$results.append(...$categoryResults);
		$category.append($results);
		return $category;
	}
}
