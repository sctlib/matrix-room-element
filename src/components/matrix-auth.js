import MatrixApi from "../services/api.js";

const template = document.createElement("template");
template.innerHTML = `
	<slot name="logged-in"></slot>
	<slot name="logged-out"></slot>
`;
export default class MatrixAuth extends HTMLElement {
	static get observedAttributes() {
		return ["auth"];
	}

	/* props attributes */
	get showGuest() {
		return this.getAttribute("show-guest") === "true";
	}
	get showUser() {
		return this.getAttribute("show-user") === "true";
	}

	/* state */
	get auth() {
		return MatrixApi.auth;
	}
	get authGuest() {
		return MatrixApi.authGuest;
	}
	get $loggedIn() {
		return (
			this.shadowRoot.querySelector('slot[name="logged-in"') ||
			this.querySelector('[slot="logged-in"]')
		);
	}
	get $loggedOut() {
		return (
			this.shadowRoot.querySelector('slot[name="logged-out"') ||
			this.querySelector('[slot="logged-out"]')
		);
	}

	attributeChangedCallback() {
		this.render();
	}
	handleAuth() {
		this.render();
	}
	constructor() {
		super();
		this.attachShadow({ mode: "open" });
		this.shadowRoot.append(template.content.cloneNode(true));
	}
	connectedCallback() {
		MatrixApi.addEventListener("auth", this.handleAuth.bind(this));
		this.render();
	}
	disconnectedCallback() {
		MatrixApi.removeEventListener("auth", this.handleAuth.bind(this));
	}
	render() {
		this.shadowRoot.replaceChildren();
		this.shadowRoot.append(
			this.auth ? this.createLoggedIn() : this.createLoggedOut()
		);
		this.showUser && this.shadowRoot.append(this.createUser());
		this.showGuest && this.shadowRoot.append(this.createUserGuest());
	}
	createLoggedIn() {
		const $slot = this.createSlot("logout");
		const $auth = document.createElement("matrix-logout");
		$auth.setAttribute("part", "logout");
		$slot.append($auth);
		return $slot;
	}
	createLoggedOut() {
		const $slot = this.createSlot("login");
		const $auth = document.createElement("matrix-login");
		$auth.setAttribute("part", "login");
		$slot.append($auth);
		return $slot;
	}
	createUser() {
		const $slot = this.createSlot("user");
		const $user = document.createElement("matrix-user");
		$user.setAttribute("part", "user");
		$slot.append($user);
		return $slot;
	}
	createUserGuest() {
		const $slot = this.createSlot("guest");
		const $user = document.createElement("matrix-user");
		$user.setAttribute("show-guest", true);
		$user.setAttribute("part", "guest");
		$slot.append($user);
		return $slot;
	}
	createSlot(name) {
		const $slot = document.createElement("slot");
		if (name) {
			$slot.setAttribute("name", name);
		}
		return $slot;
	}
}
