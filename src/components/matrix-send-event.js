import mxApi from "../services/api.js";
import { MatrixApiWidget } from "../services/api-widget.js";
import eventsManager from "../services/events-manager.js";
import MX_DOM_EVENTS from "../services/dom-events.js";
const { ON_EVENT_SEND, ON_FILE_UPLOAD } = MX_DOM_EVENTS;

const infoTemplate = document.createElement("template");
infoTemplate.innerHTML = `
	<form>
		<fieldset>
			<legend>Error</legend>
			Add a children HTML template element with a form and named inputs, for this event-type, or register it in the EventsManager
		</fieldset>
	</form>
`;

export default class MatrixSendEvent extends HTMLElement {
	/* props */
	/* a room_id or room_alias, the form will handle */
	get profileId() {
		return this.getAttribute("profile-id") || "";
	}
	get eventType() {
		return this.getAttribute("event-type");
	}
	set eventType(str) {
		return this.setAttribute("event-type", str);
	}
	get isStateEvent() {
		return eventsManager.isStateEvent(this.eventType);
	}
	get isLocal() {
		return this.getAttribute("is-local") === "true";
	}
	get isWidget() {
		return this.getAttribute("is-widget") === "true";
	}
	get widgetCapabilities() {
		const userCaps = this.getAttribute("widget-capabilities")?.split(",") || [];
		const allCaps = [
			...userCaps,
			`org.matrix.msc2762.send.event:${this.eventType}`, // room_id
		];
		if (this.eventType === "m.room.message") {
			allCaps.push("org.matrix.msc4039.upload_file");
		}
		return allCaps;
		/* `org.matrix.msc2762.send.event:${this.eventType}:${this.profileId}`, //
			 seems room_id does not work
			 https://github.com/matrix-org/matrix-spec-proposals/blob/travis/msc/widgets-send-receive-events/proposals/2762-widget-event-receiving.md*/
	}

	/* dom helpers */
	get $template() {
		return (
			this.querySelector("template") ||
			eventsManager.getFormTemplate(this.eventType) ||
			infoTemplate
		);
	}
	get $form() {
		return this.querySelector("form");
	}
	get $fieldsets() {
		return this.querySelectorAll("fieldset");
	}
	connectedCallback() {
		if (!this.eventType) {
			this.eventType = "m.room.message";
		}
		const { roomId } = mxApi.checkMatrixId(this.profileId);
		if (this.isWidget && roomId) {
			this.api = new MatrixApiWidget({
				capabilities: this.widgetCapabilities,
			});
			this.api.start();
		} else {
			this.api = mxApi;
		}
		this.addEventListener("submit", this.handleSubmit.bind(this));
		this.render();
	}
	disconnectedCallback() {
		this.removeEventListener("submit", this.handleSubmit.bind(this));
	}
	render() {
		if (this.profileId) {
			this.append(this.$template.content.cloneNode(true));
		} else {
			this.renderError("Missing profile-id with room alias/id");
		}
	}
	renderError(message = "") {
		this.append(message);
	}
	disableForm() {
		this.$fieldsets.forEach(($fieldset) => {
			$fieldset.setAttribute("disabled", true);
		});
		this.setAttribute("loading", true);
	}
	enableForm() {
		this.$fieldsets.forEach(($fieldset) =>
			$fieldset.removeAttribute("disabled")
		);
		this.removeAttribute("loading");
	}
	resetForm() {
		if (this.$form) {
			this.$form.reset();
		}
	}
	async handleSubmit(event) {
		event.preventDefault();
		const formData = new FormData(this.$form);
		this.disableForm();

		try {
			await this.handleFormData(formData);
			this.resetForm();
		} catch (e) {
			console.error("Error submitting form", e);
		}
		this.enableForm();
	}
	/* get the event content and representation from the form, post it
	 as the appropriate event */
	async handleFormData(formData) {
		const formContent = this.getFormContent(formData);
		/* if there are file(s), POST them first
			 (to get their event_id and reference it on the room message event) */
		const filePromises = Object.entries(formContent)
			.filter(([formKey, file]) => {
				return file instanceof File && file.size > 0;
			})
			.map(([eventKey, file]) => {
				if (this.isLocal) {
					return Promise.resolve({ file, eventKey });
				} else {
					return this.uploadFile(file, eventKey);
				}
			});
		/* in theory we could handle multi-file-upload (tbd); how to ref many files
			 on a roo.message event (nothing in spec)?. New event type (no client support)? */
		if (filePromises.length) {
			try {
				await Promise.all(filePromises).then((fileResponses) => {
					fileResponses.forEach(({ content_uri, fileType, eventKey, file }) => {
						/* replace on content, the File with mxc:// url, add msgtype from file.type */
						if (content_uri && fileType) {
							formContent[eventKey] = content_uri;
							if (this.eventType === "m.room.message") {
								formContent.msgtype = `m.${fileType.split("/")[0]}`;
							}
						} else if (this.isLocal && eventKey && file) {
							if (this.eventType === "m.room.message") {
								formContent.msgtype = `m.${file.type.split("/")[0]}`;
							}
						} else {
							formContent[eventKey] = null;
						}
					});
				});
			} catch (error) {
				console.info("error uploading files", error);
				return;
			}
		} else if (!formContent.msgtype && this.eventType === "m.room.message") {
			/* add the msgtype, if text message; could be part of form input[hidden] */
			formContent.msgtype = "m.text";
		}

		/* create the new matrix event with base info */
		const info = mxApi.checkMatrixId(this.profileId);
		let room_id;

		if (info.roomId) {
			room_id = info.profileId;
		} else if (!this.isWidget) {
			try {
				const roomIdinfo = await mxApi.getRoomId(info);
				room_id = roomIdinfo?.room_id;
			} catch (error) {
				console.info("Error getting room ID", error);
			}
		}

		if (!room_id) {
			throw "Incorect room-id or profile-id attributes, cannot get room info";
		}

		if (!this.eventType) {
			throw "Incorrect event-type attribute on custom event form";
		}
		const newMxEvent = {
			room_id,
			event_type: this.eventType,
			content: formContent,
		};

		/* post to API */
		let res;
		try {
			if (!this.isLocal) {
				if (this.isStateEvent) {
					res = await this.api.sendStateEvent(newMxEvent);
				} else {
					res = await this.api.sendEvent(newMxEvent);
				}

				/* associate the event ID from POST reponse */
				newMxEvent.event_id = res.event_id;

				if (res && res.error) {
					throw res;
				}
			}

			/* send JS event if no POST error */
			const newDOMEvent = new CustomEvent(ON_EVENT_SEND, {
				bubbles: true,
				detail: newMxEvent,
			});
			this.dispatchEvent(newDOMEvent);
		} catch (error) {
			console.error("Error sending event", error, newMxEvent);
		}
	}
	async uploadFile(file, eventKey) {
		if (!file || !file.size > 0) {
			return;
		}
		let fileRes;
		try {
			fileRes = await this.api.uploadFile({
				contentType: file.type,
				filename: file.name,
				file: file,
			});
			if (fileRes && fileRes.error) {
				throw fileRes;
			}
			/* send JS event if no error */
			const newFileEvent = new CustomEvent(ON_FILE_UPLOAD, {
				bubbles: true,
				detail: fileRes,
			});
			this.dispatchEvent(newFileEvent);
		} catch (error) {
			console.log("error uploading file", error, file);
			throw error;
		}
		return {
			...fileRes,
			// for updating the correct "file" input name in the form data
			eventKey,
			fileType: file.type,
		};
	}
	/* convert FormData into the content of a new event */
	getFormContent(formData) {
		return (
			Array.from(formData).reduce((acc, entry) => {
				const [key, value] = entry;
				if (value instanceof File && !value.size) {
				} else {
					acc[key] = value;
				}
				return acc;
			}, {}) || {}
		);
	}
}
