import matrixApi from "../services/api.js";
import MX_DOM_EVENTS from "../services/dom-events.js";
const { ON_AUTH_STAGE } = MX_DOM_EVENTS;

export default class MatrixRegisterEmailToken extends HTMLElement {
	get baseUrl() {
		return this.getAttribute("base-url");
	}
	get $form() {
		return this.querySelector("form");
	}
	get $fieldsets() {
		return this.querySelectorAll("fieldset");
	}
	get $submitOutput() {
		return this.querySelector("output");
	}

	/* state */
	sendAttempt = 0;
	clientSecret = null;
	sid = null;

	/* events */
	async handleSubmit(event) {
		event.preventDefault();
		event.stopPropagation();
		this.error = null;
		if (!this.sendAttempt) {
			this.sendAttempt = 1;
			this.clientSecret = this.api.newClientSecret();
		}
		const formData = new FormData(event.target);
		this.disableForm();
		try {
			const res = await this.api.requestEmailToken({
				baseUrl: this.baseUrl,
				email: formData.get("email"),
				client_secret: this.clientSecret,
				send_attempt: this.sendAttempt,
			});
			if (res?.error) {
				throw res;
			}
			if (res.sid) {
				this.sid = res.sid;
				this.dispatchEvent(
					new CustomEvent(ON_AUTH_STAGE, {
						bubbles: true,
						detail: {
							sid: res.sid,
							client_secret: this.clientSecret,
						},
					})
				);
				this.resetForm();
			}
		} catch (error) {
			console.error("error getting token", error);
			this.error = error;
			this.sendAttempt = this.sendAttempt + 1;
		}
		this.enableForm();
		this.render();
	}
	handleUpdateEmail(event) {
		event.preventDefault();
		this.sid = null;
		this.render();
	}
	/* form state */
	resetForm() {
		if (this.$form) {
			this.$form.reset();
			this.$submitOutput.replaceChildren();
		}
	}
	enableForm() {
		this.$fieldsets.forEach(($fieldset) => {
			$fieldset.removeAttribute("disabled");
		});
	}
	disableForm() {
		this.$fieldsets.forEach(($fieldset) => {
			$fieldset.setAttribute("disabled", true);
		});
	}

	/* lyfecycle */
	connectedCallback() {
		this.api = matrixApi;
		this.render();
	}
	disconnectedCallback() {
		if (this.$form) {
			this.$form.removeEventListener("submit", this.handleSubmit.bind(this));
		}
	}

	/* render DOM */
	render() {
		if (this.error) {
			const $error = this.createError(this.error);
			this.$submitOutput.replaceChildren($error);
		} else if (this.sid) {
			const $sucess = this.createSuccess();
			this.replaceChildren($sucess);
		} else if (!this.email) {
			const $form = this.createEmailForm();
			this.replaceChildren($form);
		} else {
			const $form = this.createTokenForm(this.sid);
			this.replaceChildren($form);
		}
	}
	createError(error) {
		const $error = document.createElement("matrix-error");
		$error.setAttribute("error", JSON.stringify(error));
		return $error;
	}
	createSuccess() {
		const $buttonUpdate = document.createElement("button");
		$buttonUpdate.addEventListener("click", this.handleUpdateEmail.bind(this));
		$buttonUpdate.textContent = "Update email";
		$buttonUpdate.name = "update";

		const $legend = document.createElement("legend");
		$legend.textContent = "Email address";

		const $fieldset = document.createElement("fieldset");
		$fieldset.append($legend, $buttonUpdate);

		const $nav = document.createElement("nav");
		$nav.append($fieldset);
		return $nav;
	}

	createEmailForm() {
		const $form = document.createElement("form");
		$form.addEventListener("submit", this.handleSubmit.bind(this));

		const $emailField = document.createElement("fieldset");
		const $emailLegend = document.createElement("legend");
		$emailLegend.textContent = "Email address";
		const $emailInput = document.createElement("input");
		$emailInput.type = "email";
		$emailInput.name = "email";
		$emailInput.placeholder = "username@domain.tld";
		$emailInput.required = true;
		$emailField.append($emailLegend, $emailInput);

		const $submitField = document.createElement("fieldset");
		const $submitOutput = document.createElement("output");
		const $submit = document.createElement("button");
		$submit.textContent = "Request validation link by email";
		$submit.type = "submit";
		$submitField.append($submit, $submitOutput);

		$form.append($emailField, $submitField);
		return $form;
	}

	createTokenForm() {
		const $form = document.createElement("form");
		$form.addEventListener("submit", this.handleSubmit.bind(this));

		const $tokenField = document.createElement("fieldset");
		const $tokenLegend = document.createElement("legend");
		$tokenLegend.textContent = "Email token";
		const $token = document.createElement("input");
		$token.type = "text";
		$token.name = "id_access_token";
		$token.placeholder = "Validation link received by email (with token)";
		$token.required = true;
		$tokenField.append($tokenLegend, $token);

		const $submitField = document.createElement("fieldset");
		const $submitOutput = document.createElement("output");
		const $submit = document.createElement("button");
		$submit.textContent = "Confirm email address with validation token";
		$submit.type = "submit";
		$submitField.append($submit, $submitOutput);

		$form.append($tokenField, $submitField);
		return $form;
	}
}
