import matrixApi from "../services/api.js";
import eventsManager from "../services/events-manager.js";

export const templateSelect = document.createElement("template");
templateSelect.innerHTML = `
	<matrix-send-event-select>
		<form>
			<fieldset>
				<legend>Event type</legend>
			</fieldset>
		</form>
	</matrix-send-event-select>
`;

export default class MatrixSendEvents extends HTMLElement {
	static get observedAttributes() {
		return ["event-type"];
	}
	/* props */
	get profileId() {
		return this.getAttribute("profile-id") || "";
	}
	get eventTypes() {
		return JSON.parse(this.getAttribute("event-types")) || ["m.room.message"];
	}
	/* state */
	set eventType(str) {
		this.setAttribute("event-type", str);
	}
	get eventType() {
		return this.getAttribute("event-type");
	}
	get isWidget() {
		return this.getAttribute("is-widget") === "true";
	}
	/* the active matrix-send-event from the selected event type */
	get $eventType() {
		return this.querySelector(
			`matrix-send-event[event-type="${this.eventType}"]`
		);
	}
	setEventType(eventType) {
		this.renderEventType(eventType);
		this.eventType = eventType;
	}
	onSelect(event) {
		event.preventDefault();
		this.setEventType(event.target.value);
	}
	connectedCallback() {
		if (this.eventTypes.length > 0) {
			this.eventType = this.eventTypes[0];
		}
		this.renderSelect();
		this.renderEventType(this.eventType);
	}
	renderSelect() {
		/* build event type form select */
		if (this.eventTypes.length > 1) {
			const $select = this.buildSelect();
			this.replaceChildren($select);
		}
	}
	renderEventType(eventType) {
		const $newEventType = this.buildEventTypeTemplate(eventType);
		if (this.$eventType) {
			this.$eventType.replaceWith($newEventType);
		} else {
			this.append($newEventType);
		}
	}
	buildError(err) {
		return err;
	}
	buildEventTypeTemplate(eventType) {
		const $sendEvent = document.createElement("matrix-send-event");
		$sendEvent.setAttribute("event-type", eventType);
		$sendEvent.setAttribute("profile-id", this.profileId);
		if (this.isWidget) {
			$sendEvent.setAttribute("is-widget", this.isWidget);
			if (this.eventTypes.length > 1) {
				$sendEvent.setAttribute(
					"widget-capabilities",
					"org.matrix.msc2974.request_capabilities"
				);
			}
		}

		const $template = eventsManager.getFormTemplate(eventType);
		$template && $sendEvent.append($template.cloneNode(true));

		return $sendEvent;
	}
	buildSelect() {
		const $select = document.createElement("select");
		const $options = this.eventTypes.map((option) => {
			const $option = document.createElement("option");
			$option.value = option;
			$option.innerText = option;
			if (option === this.eventType) {
				$option.setAttribute("selected", true);
			}
			return $option;
		});
		$select.append(...$options);
		$select.addEventListener("input", this.onSelect.bind(this));

		const $wrapper = templateSelect.content.cloneNode(true);
		$wrapper.querySelector("fieldset").append($select);
		return $wrapper;
	}
}
