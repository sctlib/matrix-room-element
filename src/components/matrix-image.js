import api from "../services/api.js";

export default class MatrixImage extends HTMLElement {
	static get observedAttributes() {
		return ["mxc", "title", "alt", "figcaption", "width", "height"];
	}
	get mxc() {
		return this.getAttribute("mxc") || null;
	}
	get title() {
		return this.getAttribute("title") || null;
	}
	get alt() {
		return this.getAttribute("alt") || null;
	}
	get figcaption() {
		return this.getAttribute("figcaption") || null;
	}

	/* mostly private as no effect in matrix homeserver api */
	get width() {
		return Number(this.getAttribute("width")) || 400;
	}
	get height() {
		return Number(this.getAttribute("height")) || this.width;
	}

	/* methods */
	getThumbnailUrl(width, height) {
		return api.buildThumbnailLink({
			mxcUri: this.mxc,
			width: width || this.width,
			height: height || this.height,
		});
	}
	getDownloadUrl() {
		return api.buildDownloadLink({
			mxcUri: this.mxc,
		});
	}
	attributeChangedCallback() {
		this.render();
	}
	connectedCallback() {
		this.render();
	}
	render() {
		if (this.mxc) {
			const $dom = this.createDom();
			this.replaceChildren($dom);
		} else {
			this.replaceChildren();
		}
	}
	createDom() {
		const $figure = document.createElement("figure");
		const $picture = document.createElement("picture");

		/* the img itself */
		const $img = document.createElement("img");
		$img.setAttribute("src", this.getThumbnailUrl());
		$img.setAttribute("loading", "lazy");

		this.title && $img.setAttribute("title", this.title);
		this.alt && $img.setAttribute("alt", this.alt);

		/* responsive images with alternative sources */
		const $sourceMedium = document.createElement("source");
		$sourceMedium.setAttribute(
			"srcset",
			this.getThumbnailUrl(this.width * 2, this.height * 2)
		);
		$sourceMedium.setAttribute("media", "(min-width: 40rem)");

		const $sourceDownload = document.createElement("source");
		$sourceDownload.setAttribute("srcset", this.getDownloadUrl());
		$sourceDownload.setAttribute("media", "(min-width: 60rem)");

		/* img at the end */
		$picture.append($sourceDownload, $sourceMedium, $img);
		$figure.append($picture);

		if (this.figcaption) {
			const $figcaption = document.createElement("figcaption");
			$figcaption.textContent = this.figcaption;
			$figure.append($figcaption);
		}

		return $figure;
	}
}
