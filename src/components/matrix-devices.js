import matrixApi from "../services/api.js";
import MX_DOM_EVENTS from "../services/dom-events.js";
const { ON_AUTH, ON_AUTH_FLOW } = MX_DOM_EVENTS;

const FORM_ACTIONS = [
	{
		name: "delete_devices",
		text: "Delete selected devices",
	},
];

/* an abstract component (custom element), connected to the matrix api */
export default class MatrixDevices extends HTMLElement {
	static get observedAttributes() {
		return ["devices"];
	}
	/* props */
	get devices() {
		return JSON.parse(this.getAttribute("devices"));
	}
	set devices(arr) {
		this.setAttribute("devices", JSON.stringify(arr));
	}

	/* helpers */
	get $modal() {
		return this.querySelector("wcu-modal");
	}

	/* state */
	/* unique id to have input HTML elements not-as-children of the form,
		 so the matrix-device can have their own edit form in this list.
		 https://caniuse.com/form-attribute
		 https://stackoverflow.com/a/28528359
	 */
	formId = btoa(`${MatrixDevices.name}-${Date.now()}`);
	formActions = FORM_ACTIONS;

	/* events */
	async onDevicesSelect(event) {
		event.preventDefault();
		const data = new FormData(event.target);
		const deviceIds = this.getSelectedDevices(data);
		if (deviceIds?.length) {
			if (this.isDeletingCurrentDevice(deviceIds)) {
				const confirmDelCurrent = window.confirm(
					"Deleting the current device will also logout the user"
				);
				if (!confirmDelCurrent) {
					return;
				}
			}
			try {
				const res = await this.api.deleteDevices(deviceIds);
				const { error, flows } = res;
				if (error) {
					throw error;
				} else if (flows) {
					this.onSessionRequired(res);
				} else {
					this.onDeleteDevices(res, deviceIds);
				}
			} catch (error) {
				this.error = error;
				this.render();
			}
		}
	}
	onDeleteDevices(deleteDevicesRes, removedIds) {
		if (Object.keys(deleteDevicesRes).length === 0) {
			this.devices = this.devices.filter(({ device_id }) => {
				return !removedIds.includes(device_id);
			});
		}
	}
	onSessionRequired(sessionRequiredRes) {
		const { session, flows, params, completed } = sessionRequiredRes;
		const $authFlows = document.createElement("matrix-auth-flows");
		$authFlows.addEventListener(ON_AUTH_FLOW, this.onAuthFlow.bind(this));
		session && $authFlows.setAttribute("session", session);
		flows && $authFlows.setAttribute("flows", JSON.stringify(flows));
		params && $authFlows.setAttribute("params", JSON.stringify(params));
		completed &&
			$authFlows.setAttribute("completed", JSON.stringify(completed));
		$authFlows.setAttribute("slot", "dialog");

		const $dialog = document.createElement("wcu-dialog");
		$dialog.addEventListener("close", (event) => {
			$dialog.remove();
		});
		$dialog.append($authFlows);
		this.prepend($dialog);
		$dialog.open();
	}

	async onAuth({ detail }) {
		const { key: userType } = detail;
		if (userType === "user") {
			await this.initDevices();
		}
		this.render();
	}
	async onAuthFlow(event) {
		this.$modal?.close();
	}

	/* helpers */
	getSelectedDevices(formData) {
		return Array.from(formData).reduce((acc, formItem) => {
			const [inputName, deviceId] = formItem;
			if (inputName === "device") {
				acc = [deviceId, ...acc];
			}
			return acc;
		}, []);
	}
	isCurrentDevice(device) {
		return this.api.isCurrentDeviceId(device.device_id);
	}
	isDeletingCurrentDevice(deviceIds) {
		return (
			deviceIds?.filter((id) => this.api.isCurrentDeviceId(id)).length !== 0
		);
	}

	/* lifecycle */
	attributeChangedCallback(attrName, newValue) {
		if (attrName === "devices") {
			this.render({
				devices: newValue,
			});
		}
	}
	constructor() {
		super();
		this.api = matrixApi;
		this.api.addEventListener(ON_AUTH, this.onAuth.bind(this));
	}
	async connectedCallback() {
		if (!this.devices) {
			await this.initDevices();
		}
		this.render();
	}
	disconnectedCalback() {
		this.api.removeEventListener(ON_AUTH, this.onAuth);
	}
	async initDevices() {
		this.error = null;
		try {
			const res = await matrixApi.getDevices();
			const { devices, error } = res;
			if (error) {
				throw res;
			} else if (devices) {
				this.devices = devices;
			}
		} catch (error) {
			this.error = error;
		}
	}

	render() {
		const $doms = [];
		if (this.error) {
			$doms.push(this.createError(this.error));
		}
		if (this.devices) {
			const $form = this.createDeviceSelectionForm(this.formId);
			$form.addEventListener("submit", this.onDevicesSelect.bind(this));
			const $devices = this.createDevices(this.devices);
			const $list = this.createList($devices);
			$doms.push($form, $list);
		}
		this.replaceChildren(...$doms);
	}
	createDevices(devices) {
		return devices.map((device) => {
			const $device = this.createDevice(device);
			const $deviceSelect = this.createDeviceSelect(device);
			return [$deviceSelect, $device];
		});
	}
	createDevice(device) {
		const { device_id } = device;
		const $device = document.createElement("matrix-device");
		$device.setAttribute("device", JSON.stringify(device));
		return $device;
	}
	createDeviceSelect(device) {
		const $deviceSelect = document.createElement("input");
		$deviceSelect.setAttribute("type", "checkbox");
		$deviceSelect.setAttribute("name", "device");
		$deviceSelect.setAttribute("form", this.formId);
		$deviceSelect.setAttribute("value", device.device_id);
		return $deviceSelect;
	}
	createList($doms) {
		const $list = document.createElement("menu");
		const $listItems = $doms.map(($deviceDoms) => {
			const $li = document.createElement("li");
			$li.replaceChildren(...$deviceDoms);
			return $li;
		});
		$list.replaceChildren(...$listItems);
		return $list;
	}
	createDeviceSelectionForm(formUniqueName) {
		const $form = document.createElement("form");
		$form.setAttribute("id", formUniqueName);

		const $formActions = this.formActions.map((action) => {
			const { text, name } = action;
			const $deleteDevices = document.createElement("button");
			$deleteDevices.setAttribute("name", name);
			$deleteDevices.setAttribute("type", "submit");
			$deleteDevices.textContent = text;
			return $deleteDevices;
		});
		const $fieldset = document.createElement("fieldset");
		$fieldset.append(...$formActions);
		$form.append($fieldset);
		return $form;
	}
	createError(error) {
		const $container = document.createElement("matrix-devices-error");
		const $error = document.createElement("matrix-error");
		$error.setAttribute("error", JSON.stringify(this.error));
		$container.append($error);
		return $container;
	}
}
