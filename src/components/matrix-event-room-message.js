import matrixApi from "../services/api.js";

/* render an event of type m.room.message */
export default class MatrixEventRoomMessage extends HTMLElement {
	static get observedAttributes() {
		return ["flag-sanitizer", "event"];
	}
	get flagSanitizer() {
		return this.getAttribute("flag-sanitizer") === "true";
	}
	get event() {
		return JSON.parse(this.getAttribute("event"));
	}

	async connectedCallback() {
		this.render();
	}
	render() {
		const { content } = this.event;
		const messageType = content.msgtype;

		/* event is (just) an image */
		if (messageType === "m.image") {
			return this.renderImage();
		}

		/* event is a file */
		if (messageType === "m.file") {
			return this.renderFile();
		}

		/* event is a video */
		if (messageType === "m.video") {
			return this.renderVideo();
		}

		/* event is (just) text; if it has been edited, don't show it,
			 because the "new version of the event", has its own events */
		if (messageType === "m.text" && !content["m.new_content"]) {
			return this.renderText();
		}
		if (messageType === "m.notice") {
			return this.renderNotice();
		}
	}
	renderImage() {
		const { content } = this.event;
		const $event = document.createElement("matrix-event-image");
		const $imgAvatar = document.createElement("matrix-image");
		$imgAvatar.setAttribute("mxc", content.url);
		content.body && $imgAvatar.setAttribute("figcaption", content.body);
		$event.append($imgAvatar);
		this.append($event);
	}
	renderFile() {
		const { content } = this.event;
		const dlLink = matrixApi.buildDownloadLink({
			mxcUri: content.url,
		});

		const body = content.body || "untitled-file";

		// Create a download link for the file
		const $link = document.createElement("a");
		$link.href = dlLink;
		$link.innerText = body;
		$link.download = body;
		$link.title = "Download file";

		const $event = document.createElement("matrix-event-file");
		$event.append($link);
		this.append($event);
	}

	renderVideo() {
		const { content } = this.event;
		const dlLink = matrixApi.buildDownloadLink({
			mxcUri: content.url,
		});
		const body = content.body || "untitled-video";

		// For video files, create a video element
		const $video = document.createElement("video");
		const $source = document.createElement("source");

		$source.src = dlLink;
		$source.type = content.info.mimetype;

		$video.controls = true;
		$video.append($source);

		const $caption = document.createElement("figcaption");

		const $link = document.createElement("a");
		$link.href = dlLink;
		$link.innerText = body;
		$link.download = body;

		$caption.append($link);

		const $figure = document.createElement("figure");
		$figure.append($video);
		$figure.append($caption);

		const $event = document.createElement("matrix-event-video");
		$event.append($figure);
		this.append($event);
	}

	renderText() {
		const { content } = this.event;
		const $event = document.createElement("matrix-event-text");
		if (content.formatted_body && content.format === "org.matrix.custom.html") {
			if (this.flagSanitizer && typeof Sanitizer === "function") {
				$event.innerHTML = Sanitizer.sanitize(content.formatted_body);
			} else {
				const $fakeDom = document.createElement("fake-dom");
				$fakeDom.innerHTML = content.formatted_body;
				$event.innerText = $fakeDom.innerText;
			}
		} else {
			$event.innerText = content.body;
		}
		this.append($event);
	}
	renderNotice() {
		const { content } = this.event;
		const $event = document.createElement("matrix-event-notice");
		if (content.formatted_body && content.format === "org.matrix.custom.html") {
			if (this.flagSanitizer && typeof Sanitizer === "function") {
				$event.innerHTML = Sanitizer.sanitize(content.formatted_body);
			} else {
				$event.innerText = content.body;
			}
		} else {
			$event.innerText = content.body;
		}
		this.append($event);
	}
	renderRawEvent() {
		const $event = document.createElement("matrix-event-raw");
		const $pre = document.createElement("code");
		$pre.innerText = JSON.stringify(this.event);
		$event.append($pre);
		this.append($event);
	}
}
