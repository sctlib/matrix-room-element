export default class MatrixError extends HTMLElement {
	static get observedAttributes() {
		return ["error"];
	}
	get error() {
		return JSON.parse(this.getAttribute("error")) || null;
	}
	attributeChangedCallback() {
		this.render();
	}
	connectedCallback() {
		if (this.error) {
			this.render();
		}
	}
	render() {
		this.innerHTML = "";
		if (this.error) {
			const $doms = this.createDoms();
			this.append(...$doms);
		}
	}
	createDoms() {
		const $errcode = document.createElement("matrix-error-errcode");
		const $errcodeCode = document.createElement("pre");
		$errcodeCode.innerText = this.error.errcode;
		$errcode.append($errcodeCode);

		const $error = document.createElement("matrix-error-error");
		$error.innerText = this.error.error;

		return [$errcode, $error];
	}
}
