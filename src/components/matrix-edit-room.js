import MatrixApi from "../services/api.js";

const template = document.createElement("template");
template.innerHTML = `
	<form>
		<fieldset>
			<slot name="roomInfo"></slot>
		</fieldset>
		<fieldset>
			<button type="submit">edit room</button>
		</fieldset>
	</form>
`;

export default class MatrixEditRoom extends HTMLElement {
	static get observedAttributes() {
		return ["room-id"];
	}
	get roomId() {
		return this.getAttribute("room-id");
	}
	/* the state of the form */
	state = {
		"m.room.name": "",
		"m.room.topic": "",
		"m.room.canonical_alias": "",
		"m.room.history_visibility": "",
	};

	async connectedCallback() {
		this.append(template.content.cloneNode(true));
		this.$form = this.querySelector("form");
		this.$form.addEventListener("submit", this.handleSubmit.bind(this));
		this.$fieldsets = this.querySelectorAll("fieldset");
		this.$roomInfo = this.querySelector('slot[name="roomInfo"');

		/* initial state of room? */
		if (MatrixApi.auth && this.roomId) {
			await this.setInitialState();
		}
		this.render();
	}
	async setInitialState() {
		const profile = await MatrixApi.checkMatrixId(this.roomId);
		const roomInfo = await MatrixApi.getRoomInfo(profile);
		const { alias, name, topic, history_visibility } = roomInfo.state;
		this.state = {
			"m.room.canonical_alias": alias,
			"m.room.name": name,
			"m.room.topic": topic,
			"m.room.history_visibility": history_visibility,
		};
	}

	render() {
		this.$roomInfo.innerHTML = "";

		const $name = document.createElement("input");
		$name.addEventListener("input", this.handleInput.bind(this));
		$name.type = "text";
		$name.name = "m.room.name";
		$name.value = this.state[$name.name];
		$name.placeholder = "Name";

		const $alias = document.createElement("input");
		$alias.addEventListener("input", this.handleInput.bind(this));
		$alias.type = "text";
		$alias.name = "m.room.canonical_alias";
		$alias.value = this.state[$alias.name];
		$alias.placeholder = "Alias (#alias:server.tld)";

		const $topic = document.createElement("textarea");
		$topic.addEventListener("input", this.handleInput.bind(this));
		$topic.name = "m.room.topic";
		$topic.value = this.state[$topic.name];
		$topic.placeholder = "Topic";

		const $historyVisibility = document.createElement("select");
		const options = ["invited", "joined", "shared", "world_readable"];
		options.forEach((option) => {
			const $option = document.createElement("option");
			$option.value = option;
			$option.innerText = option;
			$historyVisibility.append($option);
		});
		$historyVisibility.name = "m.room.history_visibility";
		$historyVisibility.title = "History visibility (of events in the room)";
		$historyVisibility.value = this.state[$historyVisibility.name];
		$historyVisibility.addEventListener("input", this.handleInput.bind(this));

		const $els = [$name, $alias, $topic, $historyVisibility];
		$els.forEach(($el) => {
			this.$roomInfo.append($el);
			this.$roomInfo.append(this.createFormOutput($el));
		});
	}
	createFormOutput($el) {
		const $output = document.createElement("output");
		$output.setAttribute("for", $el.name);
		$output.value = "";
		return $output;
	}

	handleInput({ target }) {
		console.log(target.value);
		const { name, value } = target;
		this.state = {
			...this.state,
			[name]: value,
		};
	}
	async handleSubmit(event) {
		event.preventDefault();
		this.disableForm();

		let res;
		try {
			res = await MatrixApi.editRoom(this.roomId, this.state);
			if (res.errcode) {
				throw res;
			}
			/* send JS event if no error */
			const newDOMEvent = new CustomEvent("mxroomedit", {
				bubbles: true,
				detail: res,
			});
			this.dispatchEvent(newDOMEvent);
			this.resetForm();
		} catch (error) {
			console.log("Error creating room", error);
			this.handleSubmitError(error);
		}
		console.log("Create room response", res);
		this.enableForm();
	}
	handleSubmitError({ errcode, error }) {
		/* reset all existing outputs */
		this.$form.querySelectorAll("output").forEach(($out) => {
			$out.innerText = "";
		});
		/* set errors on outputs */
		if (errcode === "M_ROOM_IN_USE") {
			const $o = this.$form.querySelector('output[for="alias"]');
			$o.innerText = error;
		}
	}
	disableForm() {
		this.$fieldsets.forEach(($fieldset) =>
			$fieldset.setAttribute("disabled", true)
		);
		this.setAttribute("loading", true);
	}
	enableForm() {
		this.$fieldsets.forEach(($fieldset) =>
			$fieldset.removeAttribute("disabled")
		);
		this.removeAttribute("loading");
	}
	resetForm() {
		if (this.$form) {
			this.$form.reset();
			this.setInitialState();
			this.render();
		}
	}
}
