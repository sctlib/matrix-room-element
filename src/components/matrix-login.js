import matrixApi from "../services/api.js";
import MX_DOM_EVENTS from "../services/dom-events.js";
const { ON_LOGIN } = MX_DOM_EVENTS;

export default class MatrixLogin extends HTMLElement {
	get $form() {
		return this.querySelector("form");
	}
	get $fieldsets() {
		return this.querySelectorAll("fieldset");
	}
	get $submitOutput() {
		return this.querySelector("output");
	}

	/* events */
	async handleSubmit(event) {
		event.preventDefault();
		const formData = new FormData(event.target);
		this.disableForm();
		try {
			const res = await matrixApi.login({
				user_id: formData.get("user"),
				password: formData.get("password"),
			});
			if (res.error) {
				throw res;
			}
			this.sendLoginEvent(res);
			this.resetForm();
		} catch (error) {
			this.renderError(error);
		}
		this.enableForm();
	}
	sendLoginEvent(loginRes) {
		const userEvent = new CustomEvent(ON_LOGIN, {
			bubbles: true,
			detail: {
				user: loginRes,
			},
		});
		this.dispatchEvent(userEvent);
	}

	/* form state */
	resetForm() {
		if (this.$form) {
			this.$form.reset();
			this.$submitOutput.replaceChildren();
		}
	}
	enableForm() {
		this.$fieldsets.forEach(($fieldset) => {
			$fieldset.removeAttribute("disabled");
		});
	}
	disableForm() {
		this.$fieldsets.forEach(($fieldset) => {
			$fieldset.setAttribute("disabled", true);
		});
	}

	/* lyfecycle */
	connectedCallback() {
		this.render();
	}
	disconnectedCallback() {
		if (this.$form) {
			this.$form.removeEventListener("submit", this.handleSubmit.bind(this));
		}
	}

	/* render DOM */
	render() {
		this.replaceChildren();
		const $form = this.createForm();
		this.append($form);
	}
	renderError(error) {
		const $error = document.createElement("matrix-error");
		$error.setAttribute("error", JSON.stringify(error));
		this.$submitOutput.replaceChildren($error);
	}

	createForm() {
		const $form = document.createElement("form");
		$form.addEventListener("submit", this.handleSubmit.bind(this));

		const $userField = document.createElement("fieldset");
		const $userLegend = document.createElement("legend");
		$userLegend.innerText = "User ID";
		const $user = document.createElement("input");
		$user.type = "user";
		$user.name = "user";
		$user.placeholder = "@username:homeserver.tld";
		$user.required = true;
		$userField.append($userLegend, $user);

		const $passwordField = document.createElement("fieldset");
		const $passwordLegend = document.createElement("legend");
		$passwordLegend.innerText = "User Password";
		const $password = document.createElement("input");
		$password.type = "password";
		$password.name = "password";
		$password.placeholder = "password";
		$password.required = true;
		$passwordField.append($passwordLegend, $password);

		const $submitField = document.createElement("fieldset");
		const $submitOutput = document.createElement("output");
		const $submit = document.createElement("button");
		$submit.innerText = "Login";
		$submit.type = "submit";
		$submitField.append($submit, $submitOutput);

		$form.append($userField, $passwordField, $submitField);
		return $form;
	}
}
