// vite.config.js
import { resolve } from "path";
import { defineConfig } from "vite";
import { existsSync } from "fs";
import pkg from "../../package.json";

export default defineConfig({
	appType: "mpa",
	base: "./",
	publicDir: "assets",
	build: {
		/* minify: true, */
		lib: {
			entry: resolve("index.js"),
			formats: ["es"],
			name: "mwc",
			fileName: "mwc",
		},
		rollupOptions: {
			output: {
				dir: "dist-lib",
			},
			external: Object.keys(pkg.peerDependencies || []),
		},
	},
});
