const template = document.createElement("template");
template.innerHTML = `
	<form>
		<fieldset>
			<legend>Canonical Alias</legend>
			<input
						name="alias"
						placeholder="The canonical room alias"
			/>
		</fieldset>
		<fieldset>
			<button type="submit">Set room alias</button>
		</fieldset>
	</form>
`;

export default template;
