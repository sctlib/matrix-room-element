const template = document.createElement("template");
template.innerHTML = `
	<form>
		<fieldset>
			<legend>History visibility</legend>
			<select name=history_visibility>
				<option value="invited">invited</option>
				<option value="joined">joined</option>
				<option value="shared">shared</option>
				<option value="world_readable">world_readable</option>
			</select>
		</fieldset>
		<fieldset>
			<button type="submit">Set room history visibility</button>
		</fieldset>
	</form>
`;

export default template;
