import MRoomCreateDisplay from "./m.room.create/display.js";
import MRoomCanonicalAliasDisplay from "./m.room.canonical_alias/display.js";
import MRoomCanonicalAliasForm from "./m.room.canonical_alias/form.js";
import MRoomHistoryVisibilityDisplay from "./m.room.history_visibility/display.js";
import MRoomHistoryVisibilityForm from "./m.room.history_visibility/form.js";
import MRoomMessageDisplay from "./m.room.message/display.js";
import MRoomMessageForm from "./m.room.message/form.js";
import MRoomNameDisplay from "./m.room.name/display.js";
import MRoomNameForm from "./m.room.name/form.js";
import MRoomTopicDisplay from "./m.room.topic/display.js";
import MRoomTopicForm from "./m.room.topic/form.js";
import OrgLibliRoomMessageTrackDisplay from "./org.libli.room.message.track/display.js";
import OrgLibliRoomMessageTrackForm from "./org.libli.room.message.track/form.js";

const templates = {
	"m.room.create": {
		display: MRoomCreateDisplay,
		/* no form should be used by the client to create this event */
	},
	"m.room.canonical_alias": {
		display: MRoomCanonicalAliasDisplay,
		form: MRoomCanonicalAliasForm,
	},
	"m.room.name": {
		display: MRoomNameDisplay,
		form: MRoomNameForm,
	},
	"m.room.topic": {
		display: MRoomTopicDisplay,
		form: MRoomTopicForm,
	},
	"m.room.history_visibility": {
		display: MRoomHistoryVisibilityDisplay,
		form: MRoomHistoryVisibilityForm,
	},
	"m.room.message": {
		display: MRoomMessageDisplay,
		form: MRoomMessageForm,
	},
	"org.libli.room.message.track": {
		display: OrgLibliRoomMessageTrackDisplay,
		form: OrgLibliRoomMessageTrackForm,
	},
};

export { templates as default };
