import eventsTemplates from "../templates/matrix-event/index.js";

const SPEC_STATE_EVENTS = [
	"m.room.canonical_alias",
	"m.room.name",
	"m.room.topic",
	"m.room.history_visibility",
];

class MatrixEventsManager {
	constructor() {
		this.eventTypes = new Map();
	}
	registerEventType(
		eventType,
		{ formTemplate, displayTemplate, isState } = {}
	) {
		this.eventTypes.set(eventType, {
			formTemplate,
			displayTemplate,
			isState,
		});
	}
	getFormTemplate(eventType) {
		return this.eventTypes.get(eventType)?.formTemplate;
	}
	getDisplayTemplate(eventType) {
		return this.eventTypes.get(eventType)?.displayTemplate;
	}
	isStateEvent(eventType) {
		return this.eventTypes.get(eventType)?.isState === true;
	}
}

/* create a singleton instance of the manager */
const eventsManager = new MatrixEventsManager();

Object.entries(eventsTemplates).forEach((eventToRegister) => {
	const [eventType, eventTemplates] = eventToRegister;
	eventsManager.registerEventType(eventType, {
		formTemplate: eventTemplates.form,
		displayTemplate: eventTemplates.display,
		isState: SPEC_STATE_EVENTS.includes(eventType),
	});
});

export { eventsManager as default, MatrixEventsManager, SPEC_STATE_EVENTS };
