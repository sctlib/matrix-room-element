/* errcode, error */
const ERRORS = new Map([
	[
		"MWC_WELL_KNOWN",
		"Could not get the .well-known file for this homeserver (maybe http-s)",
	],
]);

class Errors {
	constructor(errors) {
		this.errors = new Map(errors);
	}
	get(errcode) {
		const error = this.errors.get(errcode);
		return {
			errcode,
			error,
		};
	}
}
const errors = new Errors(ERRORS);

export default errors;
