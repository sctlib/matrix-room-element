import { roomStateEventsToRoomInfo } from "../utils/room.js";
import MatrixStorageApi from "./storage.js";
import MX_DOM_EVENTS from "./dom-events.js";
import errors from "./errors.js";
import { addWellKnownToUser } from "../utils/api.js";
const { ON_AUTH } = MX_DOM_EVENTS;

export const DEFAULT_BASE_URL = "https://matrix.org";
export const DEFAULT_BASE_API = "_matrix/client";

/* https://spec.matrix.org/v1.9/appendices/#identifier-grammar */
export const MATRIX_GRAMMAR = /[a-z,0-9,\-\_\.]+/;
export const MATRIX_GRAMMAR_USER = /[a-z,0-9,\-\.\=\_\/\+]+/;

/**
 * An API to communicate with a matrix homeserver (or mockup server/service-worker)
 */
export class MatrixApi extends EventTarget {
	get user() {
		return this.storage.user;
	}
	set user(user) {
		this.storage.user = user;
	}
	get guestUser() {
		return this.storage.guestUser;
	}
	set guestUser(user) {
		this.storage.guestUser = user;
	}
	get auth() {
		return typeof this.user?.access_token === "string";
	}
	get authGuest() {
		return this.guestUser && this.guestUser.access_token;
	}
	get authAny() {
		return this.auth || this.authGuest;
	}
	get accessToken() {
		let access_token;
		if (this.user?.access_token) {
			access_token = this.user.access_token;
		} else if (this.guestUser?.access_token) {
			access_token = this.guestUser.access_token;
		}
		return access_token;
	}
	get baseUrl() {
		const { well_known = {} } = this.user || {};
		const { base_url } = well_known["m.homeserver"] || {};
		return base_url || this._baseUrl;
	}
	set baseUrl(baseUrl) {
		if (baseUrl) {
			this._baseUrl = new URL(baseUrl).href;
		}
	}
	get baseApi() {
		return DEFAULT_BASE_API;
	}
	newTxnId() {
		return `mre-${Date.now()}`;
	}
	newClientSecret() {
		const array = new Uint32Array(10);
		const random = self.crypto.getRandomValues(array);
		return btoa(`${this.newTxnId()}-${random}`);
	}
	endpointRequiresAuth(endpoint) {
		const requestedRootEndpoint = endpoint.split("/")[0];
		return !["register", "login", "versions"].includes(requestedRootEndpoint);
	}
	constructor({ baseUrl, storageKey } = {}) {
		super();
		this.storage = new MatrixStorageApi(storageKey);
		this.storage.addEventListener(ON_AUTH, this.onAuth.bind(this));
		this.storage.init();
		this.baseUrl = baseUrl;
	}
	onAuth({ detail }) {
		this.dispatchEvent(
			new CustomEvent(ON_AUTH, {
				bubbles: true,
				detail,
			})
		);
	}

	/* check valid matrix id,
		 https://spec.matrix.org/v1.9/appendices/#identifier-grammar */
	checkMatrixId(profileAddress) {
		const [profileIdHash = "", profileHostname = "", profileHostPort = ""] =
			profileAddress.split(":");
		const profileType = profileIdHash.substr(0, 1);
		const profileLocal = profileIdHash.substr(1);
		const profileHost = [profileHostname, profileHostPort].join(":");
		const profileTypeChecks = {
			"@": () => {
				const correctStartChar = Boolean(
					profileLocal.substr(0, 1).match(/[a-z]/)?.length
				);
				const correctGrammar = Boolean(
					profileLocal.match(MATRIX_GRAMMAR_USER)?.length
				);
				return correctStartChar && correctGrammar;
			},
			"#": () => {
				return Boolean(profileLocal.match(MATRIX_GRAMMAR)?.length);
			},
			"!": () => true,
			$: () => true,
		};

		let hostUrl;
		try {
			hostUrl = new URL(`https://${profileHost}`);
			const isProfileTypeCheck = profileTypeChecks[profileType];
			const isProfileHostCheck = () => {
				const validHost = Boolean(
					hostUrl.host?.match(MATRIX_GRAMMAR_USER)?.length
				);
				return !!profileHost && validHost;
			};
			if (
				typeof isProfileTypeCheck !== "function" ||
				!isProfileTypeCheck() ||
				!isProfileHostCheck()
			) {
				throw profileAddress;
			}
		} catch (error) {
			// return an empty object if error, because not a profile
			return {};
		}
		const profile = {
			/* same key, here for clarity (also hostname?) */
			host: hostUrl?.host,
			/* the matrix grammar identifier, idenpendent of type */
			profileId: this.buildProfileAddress(
				profileType,
				profileLocal,
				hostUrl?.host
			),
		};

		/* each profile type has a key with "local value" only */
		if (profileType === "@") {
			profile.user = profileLocal;
		} else if (profileType === "#") {
			profile.roomAlias = profileLocal;
		} else if (profileType === "!") {
			profile.roomId = profileLocal;
		} else if (profileType === "$") {
			profile.eventId = profileLocal;
		}
		return profile;
	}

	buildProfileAddress(profileType, profileId, profileHost) {
		return profileType + profileId + ":" + profileHost;
	}

	async getRoomId({ host, roomAlias = null }) {
		if (!roomAlias || roomAlias[0] === "#") return;
		const aliasFull = this.buildProfileAddress("#", roomAlias, host);
		return await this.fetch({
			version: "r0",
			endpoint: `directory/room/${encodeURIComponent(aliasFull)}`,
		});
	}

	buildFetchUrl({ baseUrl, baseApi, version, endpoint, params }) {
		/* version can be null */
		const path = [baseApi, version, endpoint].filter((p) => !!p).join("/");
		const url = new URL(path, baseUrl);
		if (params) {
			params.forEach(([paramKey, paramVal]) => {
				if (typeof paramVal === "object") {
					url.searchParams.set(paramKey, JSON.stringify(paramVal));
				} else {
					url.searchParams.set(paramKey, paramVal);
				}
			});
		}
		return url;
	}
	/* - adds the request headers the fetch config
		 - adds access token to each request */
	buildFetchConfig(config = {}, includeAccessToken) {
		const headers = {
			...config.headers,
		};
		if (this.accessToken && includeAccessToken) {
			headers["Authorization"] = `Bearer ${this.accessToken}`;
		}
		return { ...config, headers };
	}

	async fetch({
		baseUrl = this.baseUrl,
		baseApi = this.baseApi,
		version = "",
		endpoint = "",
		params = [],
		config = {},
	} = {}) {
		/* if the endpoint needs auth, register a guest user,
			 except if the endpoint is to register a (guest) user
			 to allow fetching a Matrix homeserver directly */
		const endpointRequiresAuth = this.endpointRequiresAuth(endpoint);
		if (!this.authAny) {
			if (endpointRequiresAuth) {
				try {
					await this.registerGuestUser();
				} catch (error) {
					console.error("Error registering guest user", error);
				}
			}
		}
		const fetchUrl = this.buildFetchUrl({
			baseUrl,
			baseApi,
			version,
			endpoint,
			params,
		});
		const addCredentials = endpointRequiresAuth;
		const fetchConfig = this.buildFetchConfig(config, addCredentials);
		return await fetch(fetchUrl, fetchConfig).then((res) => res.json());
	}

	/* user interactive auth (UIA) flows stage requests */
	buildFlowStageRequest({ stage, stageData, session, username, password }) {
		const { sid, client_secret } = stageData || {};
		let stageReq = {};
		// auth done by #fallback mechanism
		if (stage === "m.login.recaptcha") {
		} else if (stage === "m.login.terms") {
		} else if (stage === "m.login.email.identity") {
			stageReq = {
				threepid_creds: { sid, client_secret },
			};
		}
		const auth = { session, ...stageReq };
		if (stage) {
			auth.type = stage;
		}
		return {
			auth,
			username,
			password,
			initial_device_display_name: "mwc-device",
		};
	}

	/* https://spec.matrix.org/v1.9/client-server-api/#post_matrixclientv3register
		 user registration kind ("ly"); needed for (m)any request to the matrix api */
	registerGuestUser(baseUrl = "") {
		return this.registerUserKind("guest", {}, baseUrl);
	}
	registerUser(userData = {}, baseUrl = "") {
		return this.registerUserKind("user", userData, baseUrl);
	}
	async registerUserKind(kind = "", userData = {}, baseUrl = "") {
		const { user_id } = userData;
		const params = kind ? [["kind", kind]] : undefined;
		let base_url, wellKnown;
		try {
			if (baseUrl) {
				base_url = baseUrl;
			} else if (user_id) {
				wellKnown = await this.getWellKnown(user_id);
				const {
					["m.homeserver"]: { base_url: baseUrl },
				} = wellKnown;
				base_url = baseUrl;
			} else {
				base_url = this.baseUrl;
			}
			if (!wellKnown) {
				wellKnown = { "m.homeserver": { base_url } };
			}
			/* returns a user or a UIA */
			const res = await this.fetch({
				baseUrl: base_url,
				version: "r0",
				endpoint: "register",
				params,
				config: {
					method: "POST",
					body: JSON.stringify(userData),
				},
			});
			if (res.error) {
				throw res;
			}
			if (kind === "guest") {
				this.guestUser = addWellKnownToUser(res, wellKnown);
			} else if (kind === "user") {
				this.user = addWellKnownToUser(res, wellKnown);
			}
			return res;
		} catch (e) {
			throw e;
		}
	}

	async registerAvailable(userId) {
		const { host, user, profileId } = this.checkMatrixId(userId);
		let baseUrl;
		try {
			const {
				["m.homeserver"]: { base_url },
			} = await this.getWellKnown(profileId);
			baseUrl = base_url;
		} catch (e) {
			throw errors.get("MWC_WELL_KNOWN");
		}

		try {
			return this.fetch({
				baseUrl,
				endpoint: "register/available",
				version: "v3",
				params: [["username", user]],
				config: {
					method: "GET",
				},
			});
		} catch (e) {
			throw e;
		}
	}

	/* spec.matrix.org/v1.9/client-server-api/#post_matrixclientv3registeremailrequesttoken */
	requestEmailToken({
		baseUrl = this.baseUrl,
		send_attempt = 1,
		client_secret = "", // newClientSecret() (paired with send_attempt)
		email,
		id_access_token,
		id_server,
		next_link,
	}) {
		return this.fetch({
			baseUrl,
			endpoint: "register/email/requestToken",
			version: "v3",
			config: {
				method: "POST",
				body: JSON.stringify({
					send_attempt,
					client_secret,
					email,
					id_access_token,
					id_server,
					next_link,
				}),
			},
		});
	}
	async login({ user_id, password }) {
		const { user } = this.checkMatrixId(user_id);
		let res;
		try {
			const wellKnown = await this.getWellKnown(user_id);
			const {
				["m.homeserver"]: { base_url },
			} = wellKnown;
			res = await this.fetch({
				baseUrl: base_url,
				endpoint: "login",
				version: "r0",
				config: {
					method: "POST",
					body: JSON.stringify({
						type: "m.login.password",
						identifier: {
							type: "m.id.user",
							user,
						},
						password,
					}),
				},
			});
			if (res.error) {
				throw res;
			}
			const userWithWellKnown = addWellKnownToUser(res, wellKnown);
			this.user = userWithWellKnown;
		} catch (e) {
			throw e;
		}
		return res;
	}
	async logout() {
		let logoutRes;
		if (this.user) {
			try {
				logoutRes = await this.fetch({
					version: "v3",
					endpoint: "logout",
					config: {
						method: "POST",
					},
				});
				this.user = null;
			} catch (e) {
				throw e;
			}
		}
		if (this.guestUser) {
			/* should not logout guest user, but let's clean it (so it resets)
				 {"errcode":"M_GUEST_ACCESS_FORBIDDEN",
				 "error":"Guest access not allowed"} */
			this.guestUser = null;
		}
		return logoutRes;
	}

	/* GET events data endpoints */
	async getRoomInfo({ roomId = "", roomAlias = "", host = "" }) {
		/* build/build full room_id (with homeserver) */
		if (roomId && host && !roomId.startsWith("!")) {
			roomId = this.buildProfileAddress("!", roomId, host);
		}
		if (!roomId && roomAlias && host) {
			const res = await this.getRoomId({
				host,
				roomAlias,
			});
			roomId = res.room_id;
		}
		if (!roomId) {
			throw {
				errcode: "MWC_ROOM_ID",
				error: "Could not fetch room info for room alias/id",
			};
		}

		/* fetch the state of the room */
		try {
			return this.getRoomStateEvents({ roomId }).then((res) => {
				if (res.error) {
					return res;
				} else {
					return roomStateEventsToRoomInfo(res);
				}
			});
		} catch (error) {
			return error;
		}
	}

	async joinRoom(id) {
		return this.fetch({
			version: "v3",
			endpoint: `rooms/${id}/join`,
			config: {
				method: "POST",
			},
		});
	}
	async leaveRoom(id) {
		return this.fetch({
			version: "v3",
			endpoint: `rooms/${id}/leave`,
			config: {
				method: "POST",
			},
		});
	}
	async forgetRoom(id) {
		return this.fetch({
			version: "v3",
			endpoint: `rooms/${id}/forget`,
			config: {
				method: "POST",
			},
		});
	}
	/* only works work authed user
		 returns result, only if user is member */
	getRoomJoinedMembers(id) {
		return this.fetch({
			version: "v3",
			endpoint: `rooms/${id}/joined_members`,
			config: {
				method: "GET",
			},
		});
	}
	/* works with guest user as well */
	getRoomMembers(id) {
		return this.fetch({
			version: "v3",
			endpoint: `rooms/${id}/members`,
			config: {
				method: "GET",
			},
		});
	}

	/* https://spec.matrix.org/v1.3/client-server-api/#creation */
	async createRoom({ name, topic, alias }) {
		const body = {
			name: name,
			topic: topic,
			room_alias_name: alias,
		};
		const res = await this.fetch({
			version: "v3",
			endpoint: "createRoom",
			config: {
				method: "POST",
				body: JSON.stringify(body),
			},
		});
		return res;
	}

	async getJoinedRooms() {
		return this.fetch({
			version: "v3",
			endpoint: `joined_rooms`,
			config: {
				method: "GET",
			},
		});
	}

	isUserJoinRoom(roomMembers) {
		if (!this.authAny) return false;
		const membershipEvent = roomMembers.find((el) => {
			if (el.content.membership !== "join") return false;
			if (this.auth && el.user_id === this.user.user_id) return true;
			if (this.authGuest && el.user_id === this.guestUser.user_id) return true;
		});
		return membershipEvent;
	}

	isUserEventSender(event) {
		const { sender } = event;
		if (!this.user || !this.user.user_id) return false;
		if (this.user.user_id === sender) return true;
	}

	/* rooms/{roomId}/send/{eventType}/{txnId}  */
	async sendEvent({ room_id, event_type, txn_id = this.newTxnId(), content }) {
		return this.fetch({
			version: "v3",
			endpoint: `rooms/${encodeURIComponent(room_id)}/send/${encodeURIComponent(
				event_type
			)}/${encodeURIComponent(txn_id)}`,
			config: {
				method: "PUT",
				body: JSON.stringify(content),
			},
		});
	}

	/* https://spec.matrix.org/v1.3/client-server-api/#put_matrixclientv3roomsroomidstateeventtypestatekeyo */
	/* /_matrix/client/v3/rooms/{roomId}/state/{eventType}/{stateKey} */
	async sendStateEvent({ room_id, event_type, state_key = "", content }) {
		return this.fetch({
			version: "v3",
			endpoint: `rooms/${encodeURIComponent(
				room_id
			)}/state/${encodeURIComponent(event_type)}/${encodeURIComponent(
				state_key
			)}`,
			config: {
				method: "PUT",
				body: JSON.stringify(content),
			},
		});
	}

	/* https://spec.matrix.org/v1.3/client-server-api/#redactions */
	async redactEvent({
		room_id,
		event_id,
		txn_id = this.newTxnId(),
		reason = "",
	}) {
		return this.fetch({
			version: "v3",
			endpoint: `rooms/${encodeURIComponent(
				room_id
			)}/redact/${encodeURIComponent(event_id)}/${encodeURIComponent(txn_id)}`,
			config: {
				method: "PUT",
				body: JSON.stringify({ reason }),
			},
		});
	}
	/* https://spec.matrix.org/v1.8/client-server-api/#event-replacements */
	async replaceEvent({
		room_id,
		event_id,
		type,
		previousContent,
		content,
		txn_id = this.newTxnId(),
	}) {
		const replacementEvent = {
			type,
			...previousContent,
			"m.new_content": content,
			"m.relates_to": {
				rel_type: "m.replace",
				event_id: event_id,
			},
		};
		return this.fetch({
			version: "v3",
			endpoint: `rooms/${encodeURIComponent(room_id)}/send/${encodeURIComponent(
				type
			)}/${encodeURIComponent(txn_id)}`,
			config: {
				method: "PUT",
				body: JSON.stringify(replacementEvent),
			},
		});
	}

	async uploadFile({ file, contentType, filename }) {
		const fileUploaded = await this.fetch({
			version: "v3",
			baseApi: "_matrix/media",
			endpoint: "upload",
			config: {
				method: "POST",
				body: file,
			},
			headers: {
				"Content-Type": contentType,
			},
			params: [["filename", filename]],
		});
		return fileUploaded;
	}
	async getRoomStateEvents({ roomId, eventType, stateKey }) {
		const enc = encodeURIComponent;
		const baseEndpoint = `rooms/${enc(roomId)}/state`;
		let endpoint;
		if (eventType && stateKey) {
			const eventTypeEndpoint = `${enc(eventType)}/${enc(stateKey)}`;
			endpoint = `${baseEndpoint}/${eventTypeEndpoint}`;
		} else {
			endpoint = baseEndpoint;
		}
		return await this.fetch({ endpoint, version: "r0" });
	}

	async getRoomMessages({ limit = 50, params = [], roomId }) {
		params = [["limit", limit], ...params];
		return await this.fetch({
			version: "r0",
			endpoint: `rooms/${encodeURIComponent(roomId)}/messages`,
			params,
		});
	}

	async getRoomEventContext({
		limit = 25, // matrix api max (default 10)
		params = [],
		roomId,
		eventId,
	}) {
		params = [
			// otherwise limit is 1/2 events_before, and 1/2 events_after
			["dir", "f"],
			["limit", limit * 2],
			...params,
		];
		return await this.fetch({
			version: "r0",
			endpoint: `rooms/${encodeURIComponent(
				roomId
			)}/context/${encodeURIComponent(eventId)}`,
			params,
		});
	}

	async gatherRoomContext({ roomId, eventId, events = [], params }) {
		const context = await this.getRoomEventContext({
			params,
			roomId,
			eventId,
			host: this.host,
		});
		const eventsAfter = context.events_after;
		if (eventsAfter && eventsAfter.length) {
			events.push(...eventsAfter);
			const lastEvent = eventsAfter[eventsAfter.length - 1];
			await this.gatherRoomContext({
				params,
				roomId,
				events,
				eventId: lastEvent.event_id,
				host: this.host,
			});
		}
		return events;
	}
	async getEvent({ roomId, eventId }) {
		return await this.fetch({
			version: "r0",
			endpoint: `rooms/${encodeURIComponent(roomId)}/event/${encodeURIComponent(
				eventId
			)}`,
		});
	}

	async getProfileEvent({ profileId, eventId }) {
		const profile = await this.checkMatrixId(profileId);
		const info = await this.getRoomInfo(profile);
		return await this.getEvent({
			roomId: info.state.id,
			eventId,
		});
	}

	/*
		 docs:
		 - https://spec.matrix.org/v1.3/client-server-api/#get_matrixclientv3sync
		 - https://javascript.info/long-polling
		 Only API that supports "long-polling"
	 */
	async sync({ params = [], signal }) {
		params.push(["timeout", 30000]);
		params.push(["limit", 100]);
		params = params.filter((param) => {
			/* serialize the since parameter, if missing, remove;
				 (this is when we're doing an "initial sync", with no reference to a previous sync ) */
			if (param[0] === "since" && !param[1]) {
				return false;
			}
			return true;
		});

		let response = await this.fetch({
			version: "v3",
			endpoint: `sync`,
			config: {
				method: "GET",
				signal,
			},
			params,
		});

		/* serialize the /sync response, as the room events (timeline),
			 do not have event.room_id, when /$room_id/context events do  */
		if (response.rooms) {
			Object.keys(response.rooms.join).forEach((roomId) => {
				response.rooms.join[roomId].timeline.events = response.rooms.join[
					roomId
				].timeline.events.map((event) => {
					event.room_id = roomId;
					return event;
				});
			});
		}
		return response;
	}

	/* only room events can be searched */
	/* https://spec.matrix.org/v1.9/client-server-api/#server-side-search */
	search({ search_categories, params }) {
		return this.fetch({
			version: "v3",
			endpoint: `search`,
			config: {
				method: "POST",
				body: JSON.stringify({ search_categories }),
				params,
			},
		});
	}

	/* https://spec.matrix.org/v1.7/client-server-api/#profiles */
	async searchUsers(search_term = "", limit = 10) {
		if (!this.auth) return;
		return this.fetch({
			version: "v3",
			endpoint: `user_directory/search`,
			config: {
				method: "POST",
				body: JSON.stringify({ search_term, limit }),
			},
		});
	}
	async getUserProfile(user_id = "") {
		return this.fetch({
			version: "v3",
			endpoint: `profile/${user_id}`,
			config: {
				method: "GET",
			},
		});
	}
	async setUserProfile(
		profileKey /* "displayname", "avatar_url" */,
		profileValue
	) {
		/* only seems to accept some keys; says arbitrary keys possible*/
		const { user_id } = this.user || this.guestUser;
		return this.fetch({
			version: "v3",
			endpoint: `profile/${user_id}/${profileKey}`,
			config: {
				method: "PUT",
				body: JSON.stringify({
					[profileKey]: profileValue,
				}),
			},
		});
	}

	/* GET /_matrix/client/v3/presence/{userId}/status
		 PUT /_matrix/client/v3/presence/{userId}/status */
	async getUserStatus(user_id = "") {
		return this.fetch({
			version: "v3",
			endpoint: `presence/${user_id}/status`,
			config: {
				method: "GET",
			},
		});
	}
	async setUserStatus({
		status_msg,
		presence /* "offline", "online", "unavailable" */,
	}) {
		if (!this.user) return;
		const { user_id } = this.user;
		return this.fetch({
			version: "v3",
			endpoint: `presence/${user_id}/status`,
			config: {
				method: "PUT",
				body: JSON.stringify({
					status_msg,
					presence,
				}),
			},
		});
	}
	async getAccountData(event_type, user_id = this.user?.user_id) {
		return this.fetch({
			version: "v3",
			endpoint: `user/${user_id}/account_data/${event_type}`,
			config: {
				method: "GET",
			},
		});
	}
	async setAccountData(event_type, body, user_id = this.user?.user_id) {
		return this.fetch({
			version: "v3",
			endpoint: `user/${user_id}/account_data/${event_type}`,
			config: {
				method: "PUT",
				body: JSON.stringify(body),
			},
		});
	}

	async getWellKnown(hostOrProfileId) {
		const { host } = this.checkMatrixId(hostOrProfileId);
		const protocol = typeof window !== "undefined" ? window.location.protocol : "https:"
		const homeserver = host
										 ? `${protocol}//${host}`
										 : hostOrProfileId;
		return fetch(new URL(`.well-known/matrix/client`, homeserver).href).then(
			(data) => {
				return data.json();
			}
		);
	}
	getVersions(baseUrl = this.baseUrl) {
		return this.fetch({ baseUrl, endpoint: "versions" });
	}

	sendToDevice({
		event_type,
		txn_id = this.newTxnId(),
		device_id = "*",
		content = {},
		user_id,
	} = {}) {
		const endpoint = `sendToDevice/${encodeURIComponent(
			event_type
		)}/${encodeURIComponent(txn_id)}`;

		return this.fetch({
			version: "v3",
			endpoint,
			config: {
				method: "PUT",
				body: JSON.stringify({
					messages: {
						[user_id]: {
							[device_id]: content,
						},
					},
				}),
			},
		});
	}

	getDevices() {
		return this.fetch({
			version: "v3",
			endpoint: "devices",
			config: {
				method: "GET",
			},
		});
	}
	getDevice(deviceId) {
		return this.fetch({
			version: "v3",
			endpoint: `devices/${deviceId}`,
			config: {
				method: "GET",
			},
		});
	}
	putDevice(deviceId, { display_name }) {
		return this.fetch({
			version: "v3",
			endpoint: `devices/${deviceId}`,
			config: {
				method: "PUT",
				body: JSON.stringify({ display_name }),
			},
		});
	}
	async deleteDevice(deviceId) {
		try {
			const res = await this.fetch({
				version: "v3",
				endpoint: `devices/${deviceId}`,
				config: {
					method: "DELETE",
				},
			});
			if (res?.error) {
				throw res;
			}
			if (this.isCurrentDeviceId(deviceId)) {
				this.logout();
			}
			return res;
		} catch (e) {
			throw e;
		}
	}
	async deleteDevices(deviceIds) {
		try {
			const res = await this.fetch({
				version: "v3",
				endpoint: "delete_devices",
				config: {
					method: "POST",
					body: JSON.stringify({
						devices: [...deviceIds],
					}),
				},
			});
			if (res?.error) {
				throw res;
			}
			const deletedCurrentDevice =
				deviceIds.filter((deviceId) => {
					return this.isCurrentDeviceId(deviceId);
				}).length !== 0;
			if (deletedCurrentDevice) {
				this.logout();
			}
			return res;
		} catch (error) {
			throw error;
		}
	}
	/* utilities for devices */
	isCurrentDeviceId(device_id) {
		return (
			device_id === this.user?.device_id ||
			device_id === this.guestUser?.device_id
		);
	}

	/* utilities for build media (img, video, files) URL from matrix URIs */
	decodeMxcUri(mxcUri) {
		if (mxcUri?.indexOf("mxc://") !== 0) {
			return {};
		}
		const mediaUrl = mxcUri.split("mxc://")[1];
		const [homeserver, id] = mediaUrl.split("/");
		return { id, homeserver };
	}
	buildDownloadLink({ mxcUri }) {
		const { id, homeserver } = this.decodeMxcUri(mxcUri);
		if (!id || !homeserver) return;
		return this.buildFetchUrl({
			baseUrl: this.baseUrl,
			baseApi: "_matrix/media",
			version: "r0",
			endpoint: `download/${homeserver}/${id}`,
		}).href;
	}
	buildThumbnailLink({ mxcUri, width = 800, height = 800, method = "scale" }) {
		const { id, homeserver } = this.decodeMxcUri(mxcUri);
		if (!id || !homeserver) return;
		return this.buildFetchUrl({
			baseUrl: this.baseUrl,
			baseApi: "_matrix/media",
			version: "v3",
			endpoint: `thumbnail/${homeserver}/${encodeURIComponent(id)}`,
			params: [
				["method", method],
				["width", width],
				["height", height],
			],
		}).href;
	}
	buildAuthFallbackUrl(authType, sessionId, baseUrl) {
		return this.buildFetchUrl({
			baseUrl,
			version: "v3",
			baseApi: "_matrix/client",
			endpoint: `auth/${encodeURIComponent(authType)}/fallback/web`,
			params: [["session", sessionId]],
		}).href;
	}
}

const api = new MatrixApi({
	baseUrl: DEFAULT_BASE_URL,
});
export default api;
