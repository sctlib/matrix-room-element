/* Mechanism, to save the "homeserver", "baseUrl" (and other keys) for a user,
	 into the local storage.
	 Some homeserver return this info alrady, do not overwrite, only add if missing,
	 so we don't need to store this info anywhere else, and otherwise not available */

export const addWellKnownToUser = (userRes, wellKnown) => {
	const user = { ...userRes };
	if (!user["well_known"]) {
		user["well_known"] = wellKnown;
	}
	return user;
};
