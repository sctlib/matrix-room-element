import groupBy from "./groupBy.js";

const roomStateEventsToRoomInfo = (roomStateEvents) => {
	/* find relevant state events and widgets */
	const roomState = {};
	const roomWidgets = [];
	const roomMembers = [];
	const spaceChildren = [];
	const spaceParents = [];

	/* go through each state events */
	if (roomStateEvents && roomStateEvents.length) {
		roomStateEvents.forEach((event) => {
			const {
				type: eventType,
				room_id,
				content: {
					alias,
					name,
					join_rule,
					topic,
					guest_access,
					history_visibility,
					url,
				},
			} = event;

			if (eventType === "m.room.create") roomState["id"] = room_id;
			if (eventType === "m.room.guest_access")
				roomState["guest_access"] = guest_access;
			if (eventType === "m.room.canonical_alias") roomState["alias"] = alias;
			if (eventType === "m.room.history_visibility")
				roomState["history_visibility"] = history_visibility;
			if (eventType === "m.room.join_rules") roomState["join_rule"] = join_rule;
			if (eventType === "m.room.topic") roomState["topic"] = topic;
			if (eventType === "m.room.avatar") roomState["avatar"] = url;
			if (eventType === "m.room.name") roomState["name"] = name;
			if (eventType === "m.room.member") roomMembers.push(event);
			if (eventType === "m.space.child") spaceChildren.push(event);
			if (eventType === "m.space.parent") spaceParents.push(event);
			if (eventType === "im.vector.modular.widgets") roomWidgets.push(event);
		});
	}

	let lastEvent;
	if (roomStateEvents.length) {
		lastEvent = roomStateEvents.find((event) => {
			return event.type === "m.room.history_visibility";
		});
	}

	/* return the room info */
	return {
		/* api v1 (not "following the matrix res") */
		state: roomState,
		widgets: roomWidgets,
		members: roomMembers,
		lastEvent,
		spaceChildren,
		spaceParents,
		/* api v2 ? (should "respect the API res" better)
		 note: maybe also "flatten (if requested)" ex: m.room_name not a list;
		 also; accessing info["m.room.name"] cannot use `?` js operator on object access;
		 maybe: return a new Map() */
		eventsByTypes: groupBy(roomStateEvents, "type"),
	};
};

export { roomStateEventsToRoomInfo };
