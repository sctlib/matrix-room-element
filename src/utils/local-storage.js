/* mockup localStorage, no used by default, when in browsers */
class MockupLocalStorage {
	storage = undefined;
	constructor() {
		this.storage = new Map();
	}
	getItem(key) {
		return this.storage.get(key) || null;
	}
	setItem(key, val) {
		return this.storage.set(key, val);
	}
	removeItem(key) {
		return this.storage.delete(key);
	}
}
/* only use the mockup local storage if in node */
if (!globalThis.localStorage) {
	globalThis.localStorage = new MockupLocalStorage();
}

const getStorageKey = (db, key) => {
	return `${db}_${key}`;
};

const getData = (db, key) => {
	return JSON.parse(localStorage.getItem(getStorageKey(db, key)));
};

const setData = (db, key, value) => {
	if (value) {
		localStorage.setItem(getStorageKey(db, key), JSON.stringify(value));
	} else {
		localStorage.removeItem(getStorageKey(db, key));
	}
};

export { getData, setData };
