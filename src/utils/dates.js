const toTimestamp = (ts) => {
	if (!ts) return;
	return new Intl.DateTimeFormat("en", {
		dateStyle: "long",
		timeStyle: "short",
	}).format(ts);
};

export { toTimestamp };
