# utils

A set of utility files and functions that haven't found their place
yet in a service (or external component).

To be used carefully as it might change.
