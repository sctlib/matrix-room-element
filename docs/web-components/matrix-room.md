# `<matrix-room>`

Displays the content of a matrix room.

## `profile-id` (required) String

The `id` or `alias` of a matrix room, prefixed with `#`, and it should also include the server part `:server.tld` of the room.

## `event-id` String

An optional matrix event ID. Specifying an `event-id`, will display
the room, and this event only.

> It is not necessary to specify `show-context`, to only show one
> event. Adding `show-context` will render the "load before" (in time)
> and "load after" (in time), buttons, at the top/bottom of the
> element's event list.

## `origin` String

The `origin` is the base URL (ex: `https://example.org`), used to
create links to content on the matrix element. For matrix.to URL, use
`https://matrix.to/#` with the trailing hash.

## `show-event-info` Boolean=false

Should each event show some information, such as the date of the event.

## `show-event-sender` Boolean=false

Should each event show information about the sender (user_id)

## `show-event-actions` Boolean=false

Should each event show some actions, such as a share, redact/edit
event (if the user is logged in and the sender of the event).

## `show-context` Boolean=false

Should we show the room's context?

## `show-send-event` Boolean=false

Should the element show a form to send events to the room.

It will only show if there is a logged in user (see `matrix-login` element), and if the user has joined the room.

It renders a `<matrix-send-events>`, setup for sending into this room,
as the logged in user, each events defined by the `send-event-types`
array.

## `send-event-types` Array=['m.room.message']

A list of events the `matrix-send-event` is allowed to send.

> Requires to register each custom event type and their associated
> `formTemplate`, to create/send a new event of these types. Also, add
> a `displayTemplate` to correctly display any custom event types.

## `show-space-children` Boolean=false

Should the element also display Matrix `spaces` that are children of the space specified in the `profile-id`.

## `flag-sanitizer` Boolean=false

Only set to true if the browser/platform has support for the `window.Sanitizer` api.

- Docs: https://developer.mozilla.org/en-US/docs/Web/API/Sanitizer
- Sanitizer polyfill: https://github.com/cure53/DOMPurify

See the `index.html` file on how to import one.

## `context-limit` Number=10

If `show-context` is true, how much element are fetched by the context API when lcicking the "load before/after" context button.

- Docs: https://spec.matrix.org/v1.3/client-server-api/#get_matrixclientv3roomsroomidcontexteventid

## `filter` Object={types, not_types, ...}

Filters (`types` and `not_types`), as specified in Matrix `EventFilter` API, for the `/messsages` and `/context` endpoints.

By default, it is set to:

```js
{
	types: ['m.room.message', 'org.libli.room.message.track'],
	not_types: [
		'm.room.redaction',
		'm.room.member',
		'libli.widget',
		'im.vector.modular.widgets',
		'im.vector.modular.widgets.libli.widget',
		'm.reaction',
		'm.room.history_visibility',
		'm.room.power_levels',
		'm.room.name',
		'm.room.topic',
		'm.space.child',
		'm.room.avatar',
	]
}
```

- matrix specification docs: https://spec.matrix.org/v1.3/client-server-api/#filtering
- see also `send-event-types` to be able to send events for a "custom type"

> For all "custom event types" we would like to display, we should
> register a `displayTemplate` with generating the HTML DOM markup of
> the event.
