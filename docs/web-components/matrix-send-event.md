# `<matrix-send-event>`

Displays a send event form interface, so a user who participates in a
room (joined), will be able to send events of type `m.room.message` by default.

When an HTML template element `<template>` is found as
children, it will be used to render the innerHTML of the send event
component.

It will expect and listen for a `submit` event, with `FormData` as for
an HTML form, when the users sends the event.

> This component works with only one children template; to handle
> multiple event-types, see `matrix-send-events` component

> Alternatively it is possible to register an event template, see
> appropriate API section.

## `event-type=""` String

The `event-type` attribute should follow the [matrix spec
naming convention](https://spec.matrix.org/latest/#events), and is
required.

> The user should have joined and be authorized to send events

### `profile-id` String=room\_(id,alias)

The room **id** or **alias** to send the event into.

Prefer the id, as the room alias will require fetching the room's id
in order to post the new event via the homeserver's API.

### `is-widget` Boolean

If set to `true` will not make a request to the api (for any type of
events). Listen to the `mxsend` event, to get the value of the form
for the event's content.

Can be usefull when this component is used as a "matrix client
widget", where the client itself handles the events (the widget will
send it through the window message api).

For more information about custom widgets see
[gitlab.com/sctlib/matrix-widgets](https://gitlab.com/sctlib/matrix-widgets/).
