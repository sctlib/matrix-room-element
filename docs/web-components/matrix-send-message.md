# `<matrix-send-message>`

Like `matrix-send-event`, but only for event type `m.room.message` (no
HTML children). Only the `profile-id` attribute is required, and
`event-type` cannot be changed.
