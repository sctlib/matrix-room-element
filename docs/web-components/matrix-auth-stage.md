# `<matrix-auth-stage>`

The stages of a `matrix-auth-flow` for the User Interactive
Authentication (UIA).

> Very early tests with the UIA

Currently should support the "auth stage types":
- `m.login.password`
- `m.login.identity`

Currently should support the [fallback methods](https://spec.matrix.org/v1.9/client-server-api/#fallback).
- `m.login.recaptcha`
- `m.login.terms`
- `m.login.email.identity`


# Attributes

- `stage` the stage type
- `session` the value of the authentication session to maintain and
  make the stage requests with
- `params` the specific params values from the UIA response from this stage type
- `completed` if the stage is already completed (can for example
  display "retry" UI)

# Events

- `MX_AUTH_STAGE` when a stage has been completed with success, the
  `details` will be an Object with the keys `{ stage, data}` where
  data can be of any type, and stage is the stage type
