# matrix-device

Calls the `/devices/:device_id`
[endpoint](https://spec.matrix.org/v1.9/client-server-api/#get_matrixclientv3devicesdeviceid)
to display a device information, using the value of the `device-id`
attribute.

If the attribute `device` is a JSON stringified Object of a device
data, it will display it, and re-render when the attribute changes.
