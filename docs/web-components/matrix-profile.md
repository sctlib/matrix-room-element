# `<matrix-profile>`

Displays a user profile; use the `user-id` attribute with a valid
[matrix user-id](https://spec.matrix.org/latest/#users), for example :
`@matthew:matrix.org`
