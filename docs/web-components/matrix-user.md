# `<matrix-user>`

Display the content of the user authentication data. Use `show-guest`
to show the guest user account instead of the "registered and logged
in" user.
