# `<matrix-error>`

Displays an error from the matrix sever. Set the attribute `error`
with a JSON stringified object that looks like:

```json
{
	"errcode": "MY_ERROR",
	"error": "My error message
}
```
