# `<matrix-room-context>`

Displays the `/context` API endpoint results for a room, with "load
before" and "load after" buttons (for the context before or after the first or last events displayed).
