# `<matrix-room-state>`

Displays a UI of all the state events for a room, such as its name, canonical alias, topic, avatar etc.
