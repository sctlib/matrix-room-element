# `<matrix-auth-flows>`

Can be used for the User Interactive Authentication (UIA), used for
example on the
[/register](https://spec.matrix.org/v1.9/client-server-api/#post_matrixclientv3register)
endpoint.

The principle is that a user wishing to accomplish an account
management action on the homserver, has to go through an
"authenciation flow", to prove its identity, and be allowed to then
accomplish the original action.

> Currently not a lot of flow and stages are supported, it is a base
> to starting working with and support all described in the spec.

## Attributes

The component accepts all attributes of the HTTP response, from an
endpoint that supports UIA.

- `flows` (required) for the flows with stages  a user has to go through
- `session` (required) the authentication session to maintain across
  all stages of the (selected) flow
- `params` the known data about a specific stages of the flows
- `error` the matrix specication error (since the HTTP request
  resolves not to an HTTP error code)
- `errcode` the matrix specification errcode


## Events

- `MX_AUTH_FLOW` when the flow is done, with a `Map` of the stages and their results
- `MX_AUTH_STAGE` when a stage is completed (sent from the `matrix-auth-stage` component)
