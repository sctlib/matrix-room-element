# `<matrix-search>`

Can perform full text search to the homeserver API (used by the sdk
api), or quick search to suggest rooms aliases and user ids known to
the logged in user (from its joined rooms or a list of events as
attribute).

## `apisearch`

Use the `apisearch` attribute to `true` in order to search the [matrix homeserver API
search
endpoint](https://spec.matrix.org/v1.9/client-server-api/#post_matrixclientv3search). It
can searche (full text search) in the all events the rooms joined by
the authenticated user.

Set the `origin` value to a `matrix-room` component "origin" attribute
value. It will be passed to display each event, result of the search
in the context of its room.

## `quicksearch`

Use the `quicksearch` attribute to `true` in order to get suggestions
from the joined rooms of the matrix user, of user ids and rooms
aliases. In can be used with the `input` event, to perform a
`api.checkMatrixId` to get a possible profile match (then get the room
informations etc.)

Use the `quicksearch-events` attribute to a JSON stringified vaue of a
list of matrix events, to be used by the quick search to build its
HTML "datalist" (that shows the suggestions).

Customize the search input's `placeholder` and `legend` text content
with these attributes.
