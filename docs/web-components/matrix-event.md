# `<matrix-event>`

Displays the content of a matrix event. It choses from existing
default events or registered custom ones which `displayTemplate` to
use, depending on the event type.
