# `<matrix-auth>`

Set `show-guest` or `show-user` to `true` to displays authentication
information for the current user (guest or logged-in/registered), and
a login/logout flow.

This component has a shadow DOM, and some slots can be used: `logout,
login, user, guest` and CSS parts with the same name.

It is possible to use this component to conditionally display content
depending on the user authentication status using the slots.
