# API and utilities

The custom api sdk and utilities exported by this module.

## Import the api/sdk

An instance of the matrix api/sdk is exported with this project.

```js
import mwc from "@sctlib/mwc";
console.log(mwc.api);
```

> Generally, explore the import value to see the available objects and classes
> (experimental, subject to many change)

## Register a new (custom) event type

It is possible to register a custom event, so it is possible to create
a new event of this type, and render it on the screen.

For this we will use HTML and/or javascript, template elements and
custom elements.

> For more examples see the `src/templates/matrix-event/\*" folder,
> where default events and examples are organized

### Requirements

Before all, we need to set the global variable
`window.MWC_MANUAL_DEFINE` to `true`, in order to register new custom
events (otherwise there is a race condition and no components can use
the custom events). This should happend in your project's code, before
importing `@sctlib/mwc` (see the examples).

### `formTemplate`

In order to be able to send custom events with the matrix-send-event(s) (and components using it), it is necessary to register the new event and its associated _formTemplate_, the form used when sending (creating) a new event of this (new custom) type.

```js
// before this code, remember to set window.MWC_MANUAL_DEFINE = true
import mwc from "@sctlib/mwc";

const customEventFormTemplate = document.createElement("template");
customEventFormTemplate.innerHTML = `<form></form>`;

mwc.eventsManager.registerEventType("io.gitlab.sctlib.mwc", {
	formTemplate: customEventFormTemplate,
});
```

### `displayTemplate`

A display template, will expect to have one children HTML element, on
which the attribute `event="` will be set with a JSON stringified
value (web components are a practical way to build a display
template).

```js
import mwc from "../../index.js";

const customEventDisplayTemplate = document.createElement("template");
customEventDisplayTemplate.innerHTML = `<mwc-event></mwc-event>`;
// mwc-event should be defined as a web-component (see examples)

mwc.eventsManager.registerEventType("io.gitlab.sctlib.mwc", {
	displayTemplate: customEventDisplayTemplate,
});
mwc.defineComponents(mwc.componentDefinitions);
```
