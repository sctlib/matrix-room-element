# dom-events

Tries to provide an interface with all the DOM events listened to and
sent by the web-components and services this library exports.

> Currently in progress of refactor (some events are not listed, and some others listed are not fullly used).

All events should be listed there, and the liste should be kept
minimal.
