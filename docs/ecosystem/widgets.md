# Matrix client widgets

See also:

- https://gitlab.com/sctlib/matrix-widgets (original repo)
- this library's `matrix-widget` component docs

# Docs

Some links that can be usefull to implementing custom matrix widgets.

# Examples

- https://github.com/matrix-org/matrix-widget-api
- https://github.com/matrix-org/simple-stickerpicker-widget/blob/master/stickerpicker.js
- https://github.com/matrix-org/matrix-spec-proposals/pull/2764 (widgets the book)

## widgets original RFC v0 google docs

- https://docs.google.com/document/d/1uPF7XWY_dXTKVKV7jZQ2KmsI19wn9-kFRgQ1tFQP7wQ/ (read in "view mode")
- also here https://github.com/matrix-org/matrix-spec-proposals/pull/2764/files#diff-30e79df7a74998ad207dd3894a975fed94daea20a3a955fd11ade7cf7c96b9f0R366-R371

## MSC2762 (send and receive events)

- https://github.com/matrix-org/matrix-spec-proposals/blob/aeadae81e2d68f76523eb61ff0ebbbd5c3202deb/proposals/2762-widget-event-receiving.md
- https://github.com/matrix-org/matrix-react-sdk/pull/5960

> Unstable prefix While this MSC is not present in the spec, clients
> and widgets should use `org.matrix.msc2762.` in place of `m.` in all
> new identifiers of this MSC.

## How a matrix client transforms widget URL palceholders

- https://github.com/matrix-org/matrix-widget-api/blob/master/src/templating/url-template.ts

Definitions: - embedding-client = the client in which our widget is embeded in - widget = the code we're embedding in an other matrix client - widget url = the URL where the widget can be found, used to embed it

The config (matrix data) passed to the widget (by the embedding
client), through the URL, is used to embed it in the client; When
adding a widget to a room we can use the following URL (as is):

The $words are replaced by the embeding client, with the correct
values of the room the widget is added to (when the widget is
rendered) That way, we get the config of the widget from the url; aka,
which room_id it is embeded in, ect.

## Action "read_events" (not implemented, use `receive_event`)

To read the timeline (events before opening the widget)

- https://github.com/matrix-org/matrix-spec-proposals/pull/2876

> This is getting rolled into
> [#2762](https://github.com/matrix-org/matrix-spec-proposals/pull/2762)
> as it's basically the same action as receiving events.

## Action "open_modal" and "close_modal"

> not yet working

- https://github.com/matrix-org/matrix-spec-proposals/blob/travis/msc/modal-widgets/proposals/2790-modal-widgets.md


# MSC links
- https://github.com/matrix-org/matrix-spec-proposals/blob/travis/msc/widget-id/proposals/2774-widget-id.md
- https://github.com/matrix-org/matrix-spec-proposals/pull/2762
- https://github.com/matrix-org/matrix-spec-proposals/blob/travis/msc/widgets-re-exchange-caps/proposals/2974-widgets-re-exchange-capabilities.md
- https://github.com/nordeck/matrix-spec-proposals/blob/nic/feat/widgetapi-user-directory-search/proposals/3973-widgetapi-user-directory-search.md
- https://github.com/nordeck/matrix-spec-proposals/blob/nic/feat/widgetapi-read-relations/proposals/3869-widgetapi-read-event-relations.md
- https://github.com/robintown/matrix-doc/blob/widget-turn-servers/proposals/3846-widget-turn-servers.md
- https://github.com/matrix-org/matrix-spec-proposals/blob/travis/msc/widgets-send-recv-toDevice/proposals/3819-to-device-messages-for-widgets.md
- https://github.com/matrix-org/matrix-spec-proposals/blob/travis/msc/widgets-re-exchange-caps/proposals/2974-widgets-re-exchange-capabilities.md
- https://github.com/matrix-org/matrix-spec-proposals/pull/2876 → MSC2762


# Interesting code examples
- https://github.com/matrix-org/matrix-widget-api/blob/master/src/interfaces/ApiVersion.ts
- https://github.com/matrix-org/matrix-widget-api/blob/master/src/interfaces/Capabilities.ts
- https://github.com/matrix-org/matrix-widget-api/blob/master/src/interfaces/WidgetApiAction.ts
