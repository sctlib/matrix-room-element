# matrix-bots

It is possible to use bots for matrix, "users that are non-human", and
can be invited to rooms, just like any user.

They can perform actions, when some other websites/services are
changed, such as with webhooks.

For examples:

- "on gitlab repository change, bot post the changes to a matrix room"
- "on matrix message, bot post message to gitlab as repository change"

It is possible to get pretty creative on how to use them. The
matrix.org official
[#thisweekinmatrix:matrix.org](https://matrix.to/#/#thisweekinmatrix:matrix.org)
room, use bots, and "emoji reaction vote" on posts, to generate the
content of the matrix.org "This week in matrix"
website (automatically selected and publish user generated, and
voted, content).

## Links

Services and tools to use exiting bots as services, or run "your-own".

- https://www.t2host.io/
- https://github.com/t2bot

## Recipes

Some practical things that can be done with exsiting bots.

### Gitlab repository new TAG/PR to matrix room message

Can be used, for example, to publish an automatic "changelog"
page/history for a project.

- Ref: https://t2bot.io/gitlab/
